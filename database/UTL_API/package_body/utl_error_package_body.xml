<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog 
	xmlns="http://www.liquibase.org/xml/ns/dbchangelog" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:n0="http://www.oracle.com/xml/ns/dbchangelog-ext" 
	xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog 
	http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.9.xsd">
	<changeSet id="3a2260abb725b5ac3402f1402777e27cb961f43e" author="(UTL_API)-Generated" failOnError="false"    >
		<n0:createOraclePackageBody objectName="UTL_ERROR" objectType="PACKAGE_BODY" ownerName="UTL_API"   >
			<n0:source><![CDATA[
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "UTL_API"."UTL_ERROR" is
   /*
   * Error messages api.
   */
   /**
   * Logger scope prefix. 
   */
   co_scope_prefix constant utl_public_types.object_name_type := lower($$plsql_unit) || '.';
   /**
   * Error page id
   */
   co_err_page_id constant integer := 9;
   /**
   * Error page ling attributes
   */
   co_link_att constant utl_public_types.text_type := 't-Button t-Button--hot t-Button--small';

   /*
   *  Message token index table record
   */
   type r_msg_token_type is record(
       token_idx pls_integer
      ,ntoken    pls_integer);

   /*
   *  Message cache table.
   */
   type t_msg_type is table of utl_messages%rowtype index by utl_public_types.idx_type;
   /*
   *  Message context cache table.
   */
   type t_ctx_type is table of utl_message_context%rowtype index by utl_public_types.idx_type;
   /*
   *  Message contexts by message idx.
   */
   type t_msg_ctx_type is table of t_ctx_type index by utl_public_types.idx_type;
   /*
   * Message tokens by message idx
   */
   type t_token_type is table of utl_message_tokens%rowtype index by utl_public_types.idx_type;
   /*
   *  Message cache table.
   */
   type t_error_msg_type is table of r_msg_type index by utl_public_types.idx_type;
   /*
   *  Message token cache table.
   */
   type t_msg_token_type is table of r_msg_token_type index by pls_integer;
   /*
   * Message cache table.
   */
   gt_msg t_msg_type;
   /*
   * Message context cache table.
   */
   gt_ctx t_ctx_type;
   /*
   * Message context index table.
   */
   gt_ctx_idx utl_collections.t_num_str_type;
   /*
   * Message Token cache table.
   */
   gt_token t_token_type;
   /*
   * Message contexts by message idx.
   */
   gt_msg_ctx t_msg_ctx_type;
   /*
   * Message token cache table
   */
   gt_msg_token t_msg_token_type;
   /*
   * Current message master.
   */
   g_message_master_id number;
   /*
   * Error report page items 
   */
   gt_page_items apex_t_varchar2;
   /*
   * Error report page item values 
   */
   gt_page_item_values apex_t_varchar2;
   /*
   * Error report page items to clear
   */
   gt_clear apex_t_varchar2;
   /*
   * Link to the error report 
   */
   g_link_txt utl_public_types.text_type;

   /**
   * Initializes the master record
   *
   * @param in_master_type Master type (JOB, APEX_SESSION, VALUE_SET, DFF, JOB_DEFINITION, SCRIPT)
   * @param in_master_pk   PK master record
   * @param in_cleanup     Cleanup already existing messages for the current master.
   */
   procedure init(in_master_type in utl_public_types.code_type
                 ,in_master_pk   in number
                 ,in_cleanup     in boolean default false) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'init';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      --
      l_dummy      number;
      l_created    utl_message_masters.created%type;
      l_created_by utl_message_masters.created_by%type;
      --  
      pragma autonomous_transaction;
   begin
      if g_message_master_id is not null
      then
         select mas.master_pk
         into   l_dummy
         from   utl_message_masters mas
         where  mas.master_type = in_master_type
         and    mas.master_pk = in_master_pk
         and    mas.id = g_message_master_id;
         --
         if in_cleanup
         then
            delete from utl_messages msg
            where  msg.mas_id = g_message_master_id;
         end if;
      else
         <<get_master>>
         begin
            select id
            into   g_message_master_id
            from   utl_message_masters mas
            where  mas.master_type = in_master_type
            and    mas.master_pk = in_master_pk;
            --
            if in_cleanup
            then
               delete from utl_messages msg
               where  msg.mas_id = g_message_master_id;
            end if;
         exception
            when no_data_found then
               l_created    := localtimestamp;
               l_created_by := utl_db_context.app_user;
               insert into utl_message_masters
                  (id
                  ,master_type
                  ,master_pk
                  ,created
                  ,created_by)
               values
                  (to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
                  ,in_master_type
                  ,in_master_pk
                  ,l_created
                  ,l_created_by)
               returning id into g_message_master_id;
         end get_master;
         commit;
      end if;
   exception
      when others then
         rollback;
         logger.append_param(p_params => lt_params, p_name => 'in_master_type', p_val => in_master_type);
         logger.append_param(p_params => lt_params, p_name => 'in_master_pk', p_val => in_master_pk);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end init;

   /**
   * Initializes the error caches
   */
   procedure init_apex is
   begin
      gt_msg.delete;
      gt_ctx.delete;
      gt_ctx_idx.delete;
      gt_token.delete;
      gt_msg_ctx.delete;
      gt_msg_token.delete;
   end init_apex;
   /**
   * Deletes the master supplied.
   *
   */
   procedure del_master(in_master_type in utl_public_types.code_type
                       ,in_master_pk   in number) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'del_master';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      --
      pragma autonomous_transaction;
   begin
      delete from utl_message_masters mas
      where  mas.master_type = in_master_type
      and    mas.master_pk = in_master_pk;
      --
      commit;
   exception
      when others then
         rollback;
         logger.append_param(p_params => lt_params, p_name => 'in_master_type', p_val => in_master_type);
         logger.append_param(p_params => lt_params, p_name => 'in_master_pk', p_val => in_master_pk);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end del_master;

   /**
   * Clears the context caches
   */
   procedure clear_context is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'clear_context';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      gt_ctx.delete();
      gt_ctx_idx.delete();
      gt_msg_ctx.delete;
   exception
      when others then
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end clear_context;

   /**
   * Adds a context value. (VARCHAR2)
   *
   * @param pc_msg_ctx_code  Context name.
   * @param pc_msg_ctx_value Context value.
   */
   procedure add_context(in_msg_ctx_code  in utl_public_types.code_type
                        ,in_msg_ctx_value in utl_public_types.text_type) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'add_context';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      --
      -- l_ctx_idx Index in G_CTX.
      -- l_ctx     Context record.
      l_ctx_idx utl_public_types.idx_type;
      l_ctx     utl_message_context%rowtype;
   begin
      if gt_ctx_idx.exists(in_msg_ctx_code)
      then
         gt_ctx(gt_ctx_idx(in_msg_ctx_code)).msg_ctx_value := in_msg_ctx_value;
      else
         l_ctx_idx           := gt_ctx.count + 1;
         l_ctx.msg_ctx_code  := in_msg_ctx_code;
         l_ctx.msg_ctx_value := in_msg_ctx_value;
         l_ctx.msg_ctx_idx   := l_ctx_idx;
         --
         if l_ctx_idx <= co_max_context
         then
            gt_ctx(l_ctx_idx) := l_ctx;
            gt_ctx_idx(in_msg_ctx_code) := l_ctx_idx;
         else
            raise_application_error(-20000, 'utl_error.add_context: Max number of contexts exceeded', true);
         end if;
      end if;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_msg_ctx_code', p_val => in_msg_ctx_code);
         logger.append_param(p_params => lt_params, p_name => 'in_msg_ctx_value', p_val => in_msg_ctx_value);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end add_context;

   /**
   * Adds a context value for the current error. (INTEGER)
   *
   * @param pc_msg_ctx_code  Context name.
   * @param pc_msg_ctx_value Context value.
   */
   procedure add_context(in_msg_ctx_code  in utl_public_types.code_type
                        ,in_msg_ctx_value in pls_integer) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'add_context';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      add_context(in_msg_ctx_code => in_msg_ctx_code, in_msg_ctx_value => ltrim(to_char(in_msg_ctx_value)));
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_msg_ctx_code', p_val => in_msg_ctx_code);
         logger.append_param(p_params => lt_params, p_name => 'in_msg_ctx_value', p_val => in_msg_ctx_value);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end add_context;

   /**
   * Adds a context value for the current error. (DATE)
   *
   * @param pc_msg_ctx_code  Context name.
   * @param pc_msg_ctx_value Context value.
   */
   procedure add_context(in_msg_ctx_code  in utl_public_types.code_type
                        ,in_msg_ctx_value in date) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'add_context';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      add_context(in_msg_ctx_code => in_msg_ctx_code, in_msg_ctx_value => utl_date.date_to_canonical(in_msg_ctx_value));
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_msg_ctx_code', p_val => in_msg_ctx_code);
         logger.append_param(p_params => lt_params, p_name => 'in_msg_ctx_value', p_val => in_msg_ctx_value);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end add_context;

   /**
   * Inserts an error message.
   *
   * @param in_msg_text  Message text.
   * @param in_item_name APEX item name calumn names are also accepted 
   */
   procedure ins(in_msg_text  in utl_public_types.text_type
                ,in_item_name in utl_public_types.apex_name_type default null) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'ins';
      --
      -- lt_params Logger parameters.
      -- l_idx     Index in G_MSG.
      -- r_msg     Message record.
      lt_params logger.tab_param;
      l_idx     utl_public_types.idx_type;
      r_msg     utl_messages%rowtype;
   begin
      r_msg.id         := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
      r_msg.created    := localtimestamp();
      r_msg.created_by := utl_db_context.app_user();
      r_msg.msg_text   := in_msg_text;
      r_msg.mas_id     := g_message_master_id;
      r_msg.item_name  := in_item_name;
      --
      l_idx := gt_msg.count() + 1;
      gt_msg(l_idx) := r_msg;
      gt_msg_ctx(l_idx) := gt_ctx;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_msg_text', p_val => in_msg_text);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end ins;

   /**
   * Inserts an encoded message.
   *
   * @param in_msg_info Encoded message.
   */
   procedure ins(in_msg_info in utl_msg_api.r_msg_info_type) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'ins';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      --
      -- r_msg        Message record.
      -- l_idx        Index in T_MSG.
      -- r_token      Token record.
      -- l_token_idx  Index in gt_token.
      -- l_token_name Current token name.
      l_idx       utl_public_types.idx_type;
      l_token_idx utl_public_types.idx_type;
      r_msg       utl_messages%rowtype;
      r_token     utl_message_tokens%rowtype;
      r_msg_token r_msg_token_type;
   begin
      l_idx := gt_msg.count() + 1;
      --
      r_msg.id             := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
      r_msg.created        := localtimestamp();
      r_msg.created_by     := utl_db_context.app_user();
      r_msg.msg_code       := in_msg_info.msg_code;
      r_msg.application_id := utl_db_context.application_id;
      r_msg.mas_id         := g_message_master_id;
      r_msg.item_name      := in_msg_info.item_name;
      --
      -- Append IN_MSG_INFO.TOKENS to T_TOKEN.
      l_token_idx := gt_token.count();
      if in_msg_info.token_idx > 0
      then
         r_msg_token.token_idx := l_token_idx + 1;
         r_msg_token.ntoken    := in_msg_info.token_idx;
         --
         gt_msg_token(l_idx) := r_msg_token;
         --
         <<tokens>>
         for idx in 0 .. in_msg_info.token_idx - 1
         loop
            r_token.token_idx   := idx;
            r_token.token_value := in_msg_info.tokens(idx);
            r_token.msg_id      := r_msg.id;
            --
            l_token_idx := l_token_idx + 1;
            gt_token(l_token_idx) := r_token;
         end loop tokens;
      end if;
      --
      gt_msg(l_idx) := r_msg;
      gt_msg_ctx(l_idx) := gt_ctx;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_msg_info.msg_code', p_val => in_msg_info.msg_code);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end ins;

   /**
   * Flushes the caches to the database.
   *
   * @param in_keep         Context entries to keep.
   * @param in_keep_master  TRUE when the master must be kept.
   */
   procedure flush(in_keep        in utl_public_types.text_type default null
                  ,in_keep_master in boolean default false) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'flush';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      --
      -- l_msg_cnt   Number of message records
      -- l_token_cnt Number of token records.
      -- l_ctx_cnt   Number of context records.
      --
      l_msg_cnt     pls_integer;
      l_token_cnt   pls_integer;
      l_ctx_cnt     pls_integer;
      lt_keep       apex_t_varchar2;
      lt_keep_value apex_t_varchar2;
      l_keep_idx    pls_integer;
      --
      pragma autonomous_transaction;
   begin
      --
      -- Contexts
      l_msg_cnt := gt_msg.count();
      for msg_idx in 1 .. l_msg_cnt
      loop
         --
         -- Set msg_id.
         l_ctx_cnt := gt_ctx.count();
         for ctx_idx in 1 .. l_ctx_cnt
         loop
            gt_msg_ctx(msg_idx)(ctx_idx).msg_id := gt_msg(msg_idx).id;
         end loop;
         --
         forall ctx_idx in 1 .. l_ctx_cnt
            insert into utl_message_context
            values gt_msg_ctx
               (msg_idx)
               (ctx_idx);
      end loop;
      --
      -- Tokens
      l_token_cnt := gt_token.count();
      forall idx_token in 1 .. l_token_cnt
         insert into utl_message_tokens
         values gt_token
            (idx_token);
      --
      -- Messages
      forall msg_idx in 1 .. l_msg_cnt
         insert into utl_messages
         values gt_msg
            (msg_idx);
      --
      commit;
      --
      gt_msg.delete();
      gt_token.delete();
      if in_keep_master and
         in_keep is not null
      then
         -- Safe the values in IN_KEEP
         lt_keep       := apex_string.split(p_str => in_keep, p_sep => ':');
         lt_keep_value := apex_t_varchar2();
         lt_keep_value.extend(lt_keep.count);
         l_keep_idx := lt_keep.first;
         <<keep_value>>
         while l_keep_idx is not null
         loop
            lt_keep_value(l_keep_idx) := gt_ctx(gt_ctx_idx(lt_keep(l_keep_idx))).msg_ctx_value;
            l_keep_idx := lt_keep.next(l_keep_idx);
         end loop keep_value;
         clear_context;
         -- Place back the safed values 
         l_keep_idx := lt_keep.first;
         <<set_back>>
         while l_keep_idx is not null
         loop
            add_context(in_msg_ctx_code => lt_keep(l_keep_idx), in_msg_ctx_value => lt_keep_value(l_keep_idx));
            l_keep_idx := lt_keep.next(l_keep_idx);
         end loop set_back;
      elsif in_keep_master
      then
         clear_context;
      elsif not in_keep_master
      then
         g_message_master_id := null;
         clear_context;
      else
         raise_application_error(-20000, 'Inconsistent arguments', true);
      end if;
   exception
      when others then
         rollback;
         logger.append_param(p_params => lt_params, p_name => 'in_keep', p_val => in_keep);
         logger.append_param(p_params => lt_params, p_name => 'in_keep_master', p_val => in_keep_master);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end flush;

   /**
   * Flushes the caches to APEX_ERROR
   */
   procedure flush_apex is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'flush_apex';
      --
      -- lt_params   Logger parameters.
      -- l_idx       Index in GT_MSG
      -- l_token_idx Index in GT_TOKEN
      -- lt_tokens   Tokens current error message
      -- l_item_name Associated APEX item
      lt_params   logger.tab_param;
      l_idx       pls_integer;
      l_token_idx pls_integer;
      lt_tokens   apex_t_varchar2;
      l_item_name utl_public_types.apex_name_type;
   begin
      l_idx := gt_msg.first;
      while l_idx is not null
      loop
         lt_tokens := apex_t_varchar2();
         lt_tokens.extend(10);
         if gt_msg_token.exists(l_idx)
         then
            l_token_idx := gt_msg_token(l_idx).token_idx;
            <<tokens>>
            for idx in 1 .. gt_msg_token(l_idx).ntoken
            loop
               lt_tokens(idx) := gt_token(l_token_idx).token_value;
               l_token_idx := l_token_idx + 1;
            end loop tokens;
         end if;
         -- NoFormat Start
         l_item_name := CASE WHEN gt_msg(l_idx).item_name IS NOT NULL THEN utl_apex_util.item_name(in_item_name => gt_msg(l_idx).item_name) ELSE NULL END;
         -- NoFormat End
         apex_error.add_error(p_error_code       => gt_msg(l_idx).msg_code
                             ,p0                 => lt_tokens(1)
                             ,p1                 => lt_tokens(2)
                             ,p2                 => lt_tokens(3)
                             ,p3                 => lt_tokens(4)
                             ,p4                 => lt_tokens(5)
                             ,p5                 => lt_tokens(6)
                             ,p6                 => lt_tokens(7)
                             ,p7                 => lt_tokens(8)
                             ,p8                 => lt_tokens(9)
                             ,p9                 => lt_tokens(10)
                             ,p_display_location => apex_error.c_inline_in_notification
                             ,p_page_item_name   => l_item_name);
         --  
         l_idx := gt_msg.next(l_idx);
      end loop;
   exception
      when others then
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end flush_apex;

   /**
   * Returns decoded (when needed) messages.
   *
   * @param in_msg_cursor Cursor retrieving the messages.
   *
   * @return Piplelined UTL_ERROR_MESSAGES_CT 
   */
   function error_message(in_msg_cursor in c_msg_cursor_type) return utl_error_messages_ct
      pipelined is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'error_m,essage';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      co_limit constant pls_integer := 1000;
      --
      -- Gets the token values
      cursor c_token(p_msg_id in number) is
         select t.token_value
         from   utl_message_tokens t
         where  t.msg_id = p_msg_id
         order  by t.token_idx;
      --
      --
      t_error_msg     t_error_msg_type;
      l_error_message utl_error_message_ot;
      t_token         utl_collections.t_str_type;
      l_msg           utl_public_types.text_type;
   begin
      <<msg_loop>>
      loop
         fetch in_msg_cursor bulk collect
            into t_error_msg limit co_limit;
         --
         <<chunk>>
         for idx in 1 .. t_error_msg.count
         loop
            if t_error_msg(idx).msg_text is not null
            then
               l_msg := t_error_msg(idx).msg_text;
            else
               utl_msg_api.set_name(in_msg_code => t_error_msg(idx).msg_code);
               open c_token(p_msg_id => t_error_msg(idx).id);
               fetch c_token bulk collect
                  into t_token;
               for token_idx in 1 .. t_token.count
               loop
                  utl_msg_api.set_token(in_token_val => t_token(token_idx));
               end loop;
               --
               close c_token;
               l_msg := utl_msg_api.get;
            end if;
            --
            l_error_message := utl_error_message_ot(mas_id => t_error_msg(idx).mas_id, id => t_error_msg(idx).id, error_msg => l_msg);
            pipe row(l_error_message);
         end loop chunk;
         --
         exit when t_error_msg.count = 0;
      end loop msg_loop;
      --
      return;
   exception
      when no_data_needed then
         raise;
      when others then
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end error_message;

   /**
   * Gets the context prompts for the master given.
   *
   * @param in_master_type Message master type to handle.
   * @param in_master_pk   Message Master PK to handle.
   * @param out_prompt1    Prompt Context Value 1.
   * @param out_prompt2    Prompt Context Value 2.
   * @param out_prompt3    Prompt Context Value 3.
   * @param out_prompt4    Prompt Context Value 4.
   * @param out_prompt5    Prompt Context Value 5.
   */
   procedure get_prompts(in_master_type utl_public_types.code_type
                        ,in_master_pk   in number
                        ,out_prompt1    out nocopy utl_public_types.long_code_type
                        ,out_prompt2    out nocopy utl_public_types.long_code_type
                        ,out_prompt3    out nocopy utl_public_types.long_code_type
                        ,out_prompt4    out nocopy utl_public_types.long_code_type
                        ,out_prompt5    out nocopy utl_public_types.long_code_type) is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'get_prompts';
      --
      -- lt_params Logger parameters.
      lt_params     logger.tab_param;
      l_lookup_type utl_public_types.code_type;
      l_prompt1     utl_public_types.long_code_type;
      l_prompt2     utl_public_types.long_code_type;
      l_prompt3     utl_public_types.long_code_type;
      l_prompt4     utl_public_types.long_code_type;
      l_prompt5     utl_public_types.long_code_type;
      /*
      * Returns the  translated prompt for the prompt code supplied.
      *
      * in_prompt_type Prompt Lookup type.
      * in_prompt_code Prompt code.
      * 
      * Return: The translated prompt or the Prompt code when the translation could not be found. 
      */
      function trans_prompt(in_prompt_type in utl_public_types.code_type
                           ,in_prompt_code in utl_public_types.code_type) return utl_public_types.long_code_type is
         --
         -- x_unit Program unit name.
         co_scope constant logger_logs.scope%type := co_scope_prefix || 'get_prompts.trans_prompt';
         --
         --
         -- lt_params Logger parameters.
         -- l_prompt The translated prompt.
         lt_params logger.tab_param;
         l_prompt  utl_public_types.long_code_type;
      begin
         if in_prompt_code is not null
         then
            l_prompt := utl_lookups_api.meaning(in_lut_code => in_prompt_type, in_code => in_prompt_code, in_must_exist => false);
         end if;
         --
         return coalesce(l_prompt, in_prompt_code);
      exception
         when others then
            logger.append_param(p_params => lt_params, p_name => 'in_prompt_type', p_val => in_prompt_type);
            logger.append_param(p_params => lt_params, p_name => 'in_prompt_code', p_val => in_prompt_code);
            logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
            raise;
      end trans_prompt;
   begin
      select ctx.ctx1
            ,ctx.ctx2
            ,ctx.ctx3
            ,ctx.ctx4
            ,ctx.ctx5
      into   l_prompt1
            ,l_prompt2
            ,l_prompt3
            ,l_prompt4
            ,l_prompt5
      from   utl_message_context_v ctx
      where  ctx.master_type = in_master_type
      and    ctx.master_pk = in_master_pk;
      --
      l_lookup_type := in_master_type || '$PROMPTS';
      --
      out_prompt1 := trans_prompt(in_prompt_type => l_lookup_type, in_prompt_code => l_prompt1);
      out_prompt2 := trans_prompt(in_prompt_type => l_lookup_type, in_prompt_code => l_prompt2);
      out_prompt3 := trans_prompt(in_prompt_type => l_lookup_type, in_prompt_code => l_prompt3);
      out_prompt4 := trans_prompt(in_prompt_type => l_lookup_type, in_prompt_code => l_prompt4);
      out_prompt5 := trans_prompt(in_prompt_type => l_lookup_type, in_prompt_code => l_prompt5);
   exception
      when no_data_found then
         logger.append_param(p_params => lt_params, p_name => 'in_master_type', p_val => in_master_type);
         logger.append_param(p_params => lt_params, p_name => 'in_master_pk', p_val => in_master_pk);
         logger.log(p_text => 'no_data_found', p_scope => co_scope, p_params => lt_params);
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_master_type', p_val => in_master_type);
         logger.append_param(p_params => lt_params, p_name => 'in_master_pk', p_val => in_master_pk);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end get_prompts;

   /**
   * Returns the URL for calling the Error Message Dialog
   *
   * @return The URL constructed 
   */
   function error_url(in_master_type in utl_public_types.code_type
                     ,in_master_pk   in number) return utl_public_types.text_type is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'error_url';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      --
      -- l_url URL Error Message Dialog.
      -- l_cnt Number of messages for this MASTER_TYPE and MASTER_PK.
      l_url utl_public_types.text_type;
      l_cnt pls_integer;
   begin
      select count(*)
      into   l_cnt
      from   utl_message_masters mas
      where  mas.master_type = in_master_type
      and    mas.master_pk = in_master_pk;
      --
      if l_cnt > 0
      then
         gt_page_item_values(1) := in_master_type;
         gt_page_item_values(2) := ltrim(to_char(in_master_pk));
         l_url := utl_apex_util.apex_url(in_page_id        => co_err_page_id
                                        ,in_request        => null
                                        ,in_var            => gt_page_items
                                        ,in_val            => gt_page_item_values
                                        ,in_clear          => gt_clear
                                        ,in_application_id => utl_db_context.application_id);
      end if;
      --
      return apex_util.prepare_url(p_url => l_url, p_checksum_type => 'SESSION');
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_master_type', p_val => in_master_type);
         logger.append_param(p_params => lt_params, p_name => 'in_master_pk', p_val => in_master_pk);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end error_url;

   /**
   * Returns the Link attributes for the Error Button.
   *
   * @param in_master_type Master Type to handle.
   * @param in_master_pk   Master PK to handle.
   *
   * @return The Link Attributes 
   */
   function error_link_att(in_master_type in utl_public_types.code_type
                          ,in_master_pk   in number) return utl_public_types.text_type is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'error_link_att';
      --
      -- lt_params Logger parameters.
      -- l_link_att Link Attributes Error button.
      -- l_cnt      Number of messages for this MASTER_TYPE and MASTER_PK.
      lt_params  logger.tab_param;
      l_link_att utl_public_types.text_type;
      l_cnt      pls_integer;
   begin
      select count(*)
      into   l_cnt
      from   utl_message_masters mas
      where  mas.master_type = in_master_type
      and    mas.master_pk = in_master_pk;
      --
      if l_cnt > 0
      then
         l_link_att := co_link_att;
      end if;
      --
      return l_link_att;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_master_type', p_val => in_master_type);
         logger.append_param(p_params => lt_params, p_name => 'in_master_pk', p_val => in_master_pk);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end error_link_att;

   /**
   * Returns the Link text for the Error Button.
   *
   * @return The link text for the Error Button 
   */
   function error_link_text(in_master_type in utl_public_types.code_type
                           ,in_master_pk   in number) return utl_public_types.text_type is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'error_link_text';
      --
      -- lt_params  Logger parameters.
      -- l_link_txt Link Text Error button.
      -- l_cnt      Number of messages for this MASTER_TYPE and MASTER_PK.
      lt_params  logger.tab_param;
      l_link_txt utl_public_types.text_type;
      l_cnt      pls_integer;
   begin
      select count(*)
      into   l_cnt
      from   utl_message_masters mas
      where  mas.master_type = in_master_type
      and    mas.master_pk = in_master_pk;
      --
      if l_cnt > 0
      then
         l_link_txt := g_link_txt;
      end if;
      --
      return l_link_txt;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_master_type', p_val => in_master_type);
         logger.append_param(p_params => lt_params, p_name => 'in_master_pk', p_val => in_master_pk);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end error_link_text;

   /**
   * Returns 1 when the Message Master supplied exists.
   *
   * @param in_master_type Master type to handle.
   * @param in_master_pk   Master PK to handle.
   * 
   * @return 1 when the error master exists, otherwise 0. 
   */
   function master_exists(in_master_type utl_public_types.code_type
                         ,in_master_pk   in number) return utl_public_types.ind_type is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'master_exists';
      --
      -- lt_params       Logger parameters.
      -- l_master_exists Return value.
      lt_params       logger.tab_param;
      l_master_exists utl_public_types.ind_type;
   begin
      <<get_master>>
      begin
         select 1
         into   l_master_exists
         from   utl_message_masters mas
         where  mas.master_type = in_master_type
         and    mas.master_pk = in_master_pk;
      exception
         when no_data_found then
            l_master_exists := 0;
      end get_master;
      --
      return l_master_exists;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_master_type', p_val => in_master_type);
         logger.append_param(p_params => lt_params, p_name => 'in_master_pk', p_val => in_master_pk);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end master_exists;

   /**
   * Checks hether there are message for the Master supplied.
   *
   * @return TRUE when there are messages otherwise FALSE 
   */
   function has_errors return boolean is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'has_errors';
      --
      -- lt_params Logger parameters.
      lt_params    logger.tab_param;
      l_cnt        pls_integer := 0;
      l_has_errors boolean;
   begin
      if gt_msg.count = 0
      then
         select count(*)
         into   l_cnt
         from   utl_messages msg
         where  msg.mas_id = g_message_master_id;
      end if;
      --
      l_has_errors := gt_msg.count > 0 or l_cnt > 0;
      --
      return l_has_errors;
   exception
      when others then
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end has_errors;
begin
   gt_page_items := apex_t_varchar2();
   gt_page_items.extend(2);
   gt_page_items(1) := utl_apex_util.item_name(in_page_id => co_err_page_id, in_item_name => 'MASTER_TYPE');
   gt_page_items(2) := utl_apex_util.item_name(in_page_id => co_err_page_id, in_item_name => 'MASTER_PK');
   gt_page_item_values := apex_t_varchar2();
   gt_page_item_values.extend(2);
   gt_clear := apex_t_varchar2();
   gt_clear.extend(2);
   gt_clear(1) := ltrim(to_char(co_err_page_id));
   gt_clear(2) := 'RIR';
   g_link_txt := utl_msg.error_btn;
end utl_error;
]]></n0:source>
		</n0:createOraclePackageBody>
	</changeSet>
</databaseChangeLog>
