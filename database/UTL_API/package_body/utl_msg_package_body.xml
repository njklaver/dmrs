<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog 
	xmlns="http://www.liquibase.org/xml/ns/dbchangelog" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:n0="http://www.oracle.com/xml/ns/dbchangelog-ext" 
	xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog 
	http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.9.xsd">
	<changeSet id="532848eced4c08ad4df37462031ff17cf30fa020" author="(UTL_API)-Generated" failOnError="false"    >
		<n0:createOraclePackageBody objectName="UTL_MSG" objectType="PACKAGE_BODY" ownerName="UTL_API"   >
			<n0:source><![CDATA[
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "UTL_API"."UTL_MSG" is
   /**
   * Generic message utilities
   */

   /*
   * Logger scope prefix. 
   */
   co_scope_prefix constant utl_public_types.object_name_type := lower($$plsql_unit) || '.';

   /*
   * Returns a generic internal error message
   *
   * in_ref PK  utl_apex_internal where the error is logged.
   *
   * @return Apex internal error message 
   */
   function apex_internal_msg(in_ref in number) return utl_public_types.text_type is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'apex_internal_msg';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name('UTL_APEX_INTERNAL');
      utl_msg_api.set_token(to_char(in_ref));
      --
      return utl_msg_api.get;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_ref', p_val => in_ref);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end apex_internal_msg;

   /*
   * Extracts the constraint name from the Oracle error message supplied.
   *
   * @return The contraint name found 
   */
   function get_constraint(in_sqlerrm in utl_public_types.text_type) return utl_public_types.object_name_type is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'get_constraint';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      --
      -- l_str            lt_str(1) will contain the constraint_name (including the DB schema.
      -- l_delim          Split delimiters "(" and ")".
      -- l_msg_templ_code Constraint error code.
      l_str            utl_collections.t_str_type;
      l_delim          utl_string.t_delim_type;
      l_msg_templ_code utl_public_types.code_type;
   begin
      l_delim(0) := '(';
      l_delim(1) := ')';
      l_str := utl_string.split(in_str => in_sqlerrm, in_delim => l_delim, in_start_pos => false, in_end_pos => false);
      --
      if l_str.exists(1)
      then
         --
         -- Strip the schema name.
         l_msg_templ_code := ltrim(regexp_substr(l_str(1), '\..*'), '.');
      end if;
      --
      return l_msg_templ_code;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_sqlerrm', p_val => in_sqlerrm);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end get_constraint;

   /**
   * Logs an internal Apex error.
   *
   * in_error Error to log.
   * in_info  Additional info.
   *
   * Returns Generic message to be shown 
   */
   function apex_internal(in_error in apex_error.t_error
                         ,in_info  in utl_public_types.text_type default null) return utl_public_types.text_type is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'apex_internal';
      --
      -- lt_params Logger parameters.
      -- lr_err    UTL_APEX_ERROR record. 
      lt_params logger.tab_param;
      lr_err    utl_apex_error%rowtype;
      --
      pragma autonomous_transaction;
   begin
      lr_err.id               := to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
      lr_err.session_id       := utl_db_context.session_id;
      lr_err.creation_date    := localtimestamp;
      lr_err.message          := in_error.message;
      lr_err.additional_info  := coalesce(in_info, in_error.additional_info);
      lr_err.association_type := in_error.association_type;
      lr_err.page_item_name   := in_error.page_item_name;
      lr_err.region_id        := in_error.region_id;
      lr_err.column_alias     := in_error.column_alias;
      lr_err.row_num          := in_error.row_num;
      lr_err.apex_error_code  := in_error.apex_error_code;
      lr_err.ora_sqlcode      := in_error.ora_sqlcode;
      lr_err.ora_sqlerrm      := in_error.ora_sqlerrm;
      lr_err.error_backtrace  := in_error.error_backtrace;
      lr_err.error_statement  := in_error.error_statement;
      lr_err.comp_type        := in_error.component.type;
      lr_err.comp_id          := in_error.component.id;
      lr_err.comp_name        := in_error.component.name;
      --
      insert into utl_apex_error
      values lr_err;
      commit;
      --
      return apex_internal_msg(in_ref => lr_err.id);
   exception
      when others then
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end apex_internal;

   /**
   * Apex error function
   *
   * @param in_error Error to handle.
   *
   * @return Apex error result structure.
   */
   function apex_error_handler(p_error in apex_error.t_error) return apex_error.t_error_result is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'apex_error_handler';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
      --
      -- lr_result Apex error result to return.
      -- lr_error  Copy of P_ERROR to fill ORA_SQLERRM and ORA_SQLCODE.
      lr_result apex_error.t_error_result;
      lr_error  apex_error.t_error := p_error;
   begin
      lr_result := apex_error.init_error_result(lr_error);
      --
      if lr_error.message like 'ORA%'
      then
         lr_error.ora_sqlerrm := lr_error.message;
         lr_error.ora_sqlcode := to_number(substr(lr_error.message, 4, 6));
      end if;
      --
      if lr_error.is_internal_error
      then
         lr_result.display_location := apex_error.c_inline_in_notification;
         lr_result.message          := apex_internal(lr_error);
         lr_result.additional_info  := null;
      else
         if lr_error.ora_sqlcode = -20000
         then
            lr_result.display_location := apex_error.c_on_error_page;
            lr_result.message          := apex_internal(lr_error);
            lr_result.additional_info  := null;
         elsif lr_error.ora_sqlcode between - 20999 and - 20001
         then
            lr_result.display_location := apex_error.c_inline_in_notification;
            lr_result.message          := ltrim(lr_error.ora_sqlerrm, 'ORA-' || to_char(lr_error.ora_sqlcode) || ': ');
            lr_result.additional_info  := null;
         elsif lr_error.ora_sqlcode in (-1, -2290, -2291, -2292)
         then
            lr_result.display_location := apex_error.c_inline_in_notification;
            lr_result.message          := constraint_msg(in_sqlerrm => lr_error.ora_sqlerrm);
         end if;
         if lr_result.page_item_name is null and
            lr_result.column_alias is null
         then
            apex_error.auto_set_associated_item(p_error => lr_error, p_error_result => lr_result);
         end if;
      end if;
      --
      return lr_result;
   exception
      when others then
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end apex_error_handler;

   /**
   * Returns the message text for a constraint error.
   *
   * @param pc_sqlerrm Oracle error message containing the contraint error.
   *
   * @return The message text
   */
   function constraint_msg(in_sqlerrm in utl_public_types.text_type) return utl_public_types.text_type is
      --
      -- co_scope Logger scope
      co_scope constant logger_logs.scope%type := co_scope_prefix || 'constraint_msg';
      --
      -- lt_params  Logger parameters.
      -- l_msg_code Message code.
      -- l_msg      Message to return.
      lt_params  logger.tab_param;
      l_msg_code utl_public_types.code_type;
      l_msg      utl_public_types.text_type;
   begin
      l_msg_code := get_constraint(in_sqlerrm => in_sqlerrm);
      if l_msg_code is not null
      then
         utl_msg_api.set_name(in_msg_code => l_msg_code);
         l_msg := utl_msg_api.get();
      else
         l_msg := in_sqlerrm;
      end if;
      --
      return l_msg;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_sqlerrm', p_val => in_sqlerrm);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end constraint_msg;

   /**
   * Returns a NOT_SUPPORTED error message.
   *
   * @param in_type_name Object type. 
   * @param in_unit      Member unit name.
   *
   * @return NOT_SUPPORTED error message 
   */
   function not_supported(in_type_name in utl_public_types.object_name_type
                         ,in_unit_name in utl_public_types.object_name_type) return utl_public_types.text_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope         constant logger_logs.scope%type := co_scope_prefix || 'not_supported';
      co_not_supported constant utl_public_types.code_type := 'UTL_NOT_SUPPORTED';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_not_supported);
      utl_msg_api.set_token(in_token_val => in_type_name);
      utl_msg_api.set_token(in_token_val => in_unit_name);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_type_name', p_val => in_type_name);
         logger.append_param(p_params => lt_params, p_name => 'in_unit_name', p_val => in_unit_name);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end not_supported;

   /**
   * Returns a REQUIRED error message.
   *
   * @param in_attribute Attribute name
   *
   * @return REQUIRED error message 
   */
   function required(in_attribute in utl_public_types.object_name_type) return utl_public_types.text_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope    constant logger_logs.scope%type := co_scope_prefix || 'required';
      co_required constant utl_public_types.code_type := 'UTL_REQUIRED';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_required);
      utl_msg_api.set_token(in_token_val => in_attribute);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_argument', p_val => in_attribute);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end required;

   /**
   * Returns a REQUIRED error message.
   *
   * @param in_attribute Attribute name
   *
   * @return REQUIRED error message 
   */
   function required_info(in_attribute in utl_public_types.object_name_type) return utl_msg_api.r_msg_info_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope    constant logger_logs.scope%type := co_scope_prefix || 'required';
      co_required constant utl_public_types.code_type := 'UTL_REQUIRED';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_required);
      utl_msg_api.set_token(in_token_val => in_attribute);
      --
      return utl_msg_api.get_encoded();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_argument', p_val => in_attribute);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end required_info;

   /**
   * Returns a MUST_NULL error message.
   *
   * @param in_attribute Attribute name
   *
   * @return MUST_NULL error message 
   */
   function must_null(in_argument in utl_public_types.object_name_type) return utl_public_types.text_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope     constant logger_logs.scope%type := co_scope_prefix || 'required';
      co_must_null constant utl_public_types.code_type := 'UTL_MUST_NULL';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_must_null);
      utl_msg_api.set_token(in_token_val => in_argument);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_argument', p_val => in_argument);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end must_null;

   /**
   * Sets a generic NOT_FOUND message.
   *
   * @param in_entity    Entity to be found.
   * @param in_attribute Attribute to be found.
   * @param in_value     Value not found.
   *
   * @return NOTFOUND error message 
   */
   function notfound_message(in_entity    in utl_public_types.object_name_type default null
                            ,in_attribute in utl_public_types.object_name_type
                            ,in_value     in utl_public_types.text_type) return utl_public_types.text_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope     constant logger_logs.scope%type := co_scope_prefix || 'not_found';
      co_notfound1 constant utl_public_types.code_type := 'UTL_NOTFOUND1';
      co_notfound2 constant utl_public_types.code_type := 'UTL_NOTFOUND2';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      if in_entity is not null
      then
         utl_msg_api.set_name(in_msg_code => co_notfound1);
         utl_msg_api.set_token(in_entity);
         utl_msg_api.set_token(in_attribute);
         utl_msg_api.set_token(in_value);
      else
         utl_msg_api.set_name(in_msg_code => co_notfound2);
         utl_msg_api.set_token(in_attribute);
         utl_msg_api.set_token(in_value);
      end if;
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_entity', p_val => in_entity);
         logger.append_param(p_params => lt_params, p_name => 'in_attribute', p_val => in_attribute);
         logger.append_param(p_params => lt_params, p_name => 'in_value', p_val => in_value);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end notfound_message;

   /**
   * Sets a generic NOT_FOUND message.
   *
   * @param in_entity    Entity to be found.
   * @param in_attribute Attribute to be found.
   * @param in_value     Value not found.
   *
   * @return NOTFOUND error message 
   */
   function notfound_msg_info(in_entity    in utl_public_types.object_name_type default null
                             ,in_attribute in utl_public_types.object_name_type
                             ,in_value     in utl_public_types.text_type) return utl_msg_api.r_msg_info_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope     constant logger_logs.scope%type := co_scope_prefix || 'not_found';
      co_notfound1 constant utl_public_types.code_type := 'UTL_NOTFOUND1';
      co_notfound2 constant utl_public_types.code_type := 'UTL_NOTFOUND2';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      if in_entity is not null
      then
         utl_msg_api.set_name(in_msg_code => co_notfound1);
         utl_msg_api.set_token(in_entity);
         utl_msg_api.set_token(in_attribute);
         utl_msg_api.set_token(in_value);
      else
         utl_msg_api.set_name(in_msg_code => co_notfound2);
         utl_msg_api.set_token(in_attribute);
         utl_msg_api.set_token(in_value);
      end if;
      --
      return utl_msg_api.get_encoded;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_entity', p_val => in_entity);
         logger.append_param(p_params => lt_params, p_name => 'in_attribute', p_val => in_attribute);
         logger.append_param(p_params => lt_params, p_name => 'in_value', p_val => in_value);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end notfound_msg_info;

   /**
   * Sets a generic NOT_DEFINED message.
   *
   * @param in_entity    Entity to be found.
   * @param in_attribute Attribute to be found.
   * @param in_value     Value not found.
   *
   * @return NOT_DEFINED error message 
   */
   function notdefined_msg_info(in_attribute in utl_public_types.object_name_type
                               ,in_value     in utl_public_types.text_type) return utl_msg_api.r_msg_info_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope      constant logger_logs.scope%type := co_scope_prefix || 'not_found';
      co_notdefined constant utl_public_types.code_type := 'UTL_NOTDEFINED';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_notdefined);
      utl_msg_api.set_token(in_attribute);
      utl_msg_api.set_token(in_value);
      --
      return utl_msg_api.get_encoded;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_attribute', p_val => in_attribute);
         logger.append_param(p_params => lt_params, p_name => 'in_value', p_val => in_value);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end notdefined_msg_info;

   /**
   * Sets a generic INVALID_DATATYPE message.
   *
   * @param in_col       Column name
   *
   * @return NOTFOUND error message 
   */
   function invalid_datatype(in_col in utl_public_types.object_name_type) return utl_public_types.text_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope            constant logger_logs.scope%type := co_scope_prefix || 'invalid_datatype';
      co_invalid_datatype constant utl_public_types.code_type := 'UTL_INVALID_DATATYPE';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_invalid_datatype);
      utl_msg_api.set_token(in_col);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_col', p_val => in_col);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end invalid_datatype;

   /**
   * Sets a generic COL_MAX_LENGTH message.
   *
   * @param in_col       Column name
   *
   * @return UTL_COL_MAX_LENGTH error message 
   */
   function col_max_length(in_col in utl_public_types.object_name_type) return utl_public_types.text_type is
      --
      -- co_scope         Logger scope
      -- co_not_supported Message code.
      co_scope          constant logger_logs.scope%type := co_scope_prefix || 'col_max_length';
      co_col_max_length constant utl_public_types.code_type := 'UTL_COL_MAX_LENGTH';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_col_max_length);
      utl_msg_api.set_token(in_col);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_col', p_val => in_col);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end col_max_length;

   /**
   * Sets a generic DOMAIN_ERROR message.
   *
   * @param in_col          Column being checked.
   * @param in_lut_code     Domain code (Lookup Type code).
   * @param in_lut_meaning  Domain meaning (Lookup Type meaning).
   * @param in_lup_code     Value to check (Lookup Code).
   *
   * @return NOTFOUND error message 
   */
   function domain_error(in_col         in utl_public_types.object_name_type default null
                        ,in_lut_code    in utl_public_types.code_type
                        ,in_lut_meaning in utl_public_types.long_code_type
                        ,in_lup_code    in utl_public_types.code_type) return utl_public_types.text_type is
      --
      -- co_scope            Logger scope
      -- co_domain_error_col Message code.
      -- co_domain_error     Message code.
      co_scope            constant logger_logs.scope%type := co_scope_prefix || 'domain_error';
      co_domain_error_col constant utl_public_types.code_type := 'UTL_DOMAIN_ERROR_COL';
      co_domain_error     constant utl_public_types.code_type := 'UTL_DOMAIN_ERROR';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      if in_col is null
      then
         utl_msg_api.set_name(in_msg_code => co_domain_error);
         utl_msg_api.set_token(in_lut_code);
         utl_msg_api.set_token(in_lut_meaning);
         utl_msg_api.set_token(in_lup_code);
      else
         utl_msg_api.set_name(in_msg_code => co_domain_error_col);
         utl_msg_api.set_token(in_col);
         utl_msg_api.set_token(in_lut_code);
         utl_msg_api.set_token(in_lut_meaning);
         utl_msg_api.set_token(in_lup_code);
      end if;
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_col', p_val => in_col);
         logger.append_param(p_params => lt_params, p_name => 'in_lut_code', p_val => in_lut_code);
         logger.append_param(p_params => lt_params, p_name => 'in_lut_meaning', p_val => in_lut_meaning);
         logger.append_param(p_params => lt_params, p_name => 'in_lup_code', p_val => in_lup_code);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end domain_error;

   /**
   * Sets a generic DOMAIN_ERROR message.
   *
   * @param in_col          Column being checked.
   * @param in_lut_code     Domain code (Lookup Type code).
   * @param in_lut_meaning  Domain meaning (Lookup Type meaning).
   * @param in_lup_code     Value to check (Lookup Code).
   *
   * @return Encoded domain error message 
   */
   function domain_error_info(in_col         in utl_public_types.object_name_type default null
                             ,in_lut_code    in utl_public_types.code_type
                             ,in_lut_meaning in utl_public_types.long_code_type
                             ,in_lup_code    in utl_public_types.code_type) return utl_msg_api.r_msg_info_type is
      --
      -- co_scope            Logger scope
      -- co_domain_error_col Message code.
      -- co_domain_error     Message code.
      co_scope            constant logger_logs.scope%type := co_scope_prefix || 'domain_error_info';
      co_domain_error_col constant utl_public_types.code_type := 'UTL_DOMAIN_ERROR_COL';
      co_domain_error     constant utl_public_types.code_type := 'UTL_DOMAIN_ERROR';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      if in_col is null
      then
         utl_msg_api.set_name(in_msg_code => co_domain_error);
         utl_msg_api.set_token(in_lut_code);
         utl_msg_api.set_token(in_lut_meaning);
         utl_msg_api.set_token(in_lup_code);
      else
         utl_msg_api.set_name(in_msg_code => co_domain_error_col);
         utl_msg_api.set_token(in_col);
         utl_msg_api.set_token(in_lut_code);
         utl_msg_api.set_token(in_lut_meaning);
         utl_msg_api.set_token(in_lup_code);
      end if;
      --
      return utl_msg_api.get_encoded();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_col', p_val => in_col);
         logger.append_param(p_params => lt_params, p_name => 'in_lut_code', p_val => in_lut_code);
         logger.append_param(p_params => lt_params, p_name => 'in_lut_meaning', p_val => in_lut_meaning);
         logger.append_param(p_params => lt_params, p_name => 'in_lup_code', p_val => in_lup_code);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end domain_error_info;

   /**
   * Returns a generic INVALID message.
   *
   * @param in_item      Item involved.
   * @param in_value     Errogenous value.
   * @param in_value_exp Expected value (optional).           
   *
   * @return INVALID error message 
   */
   function invalid(in_item      in utl_public_types.object_name_type
                   ,in_value     in utl_public_types.text_type
                   ,in_value_exp in utl_public_types.text_type default null) return utl_public_types.text_type is
      --
      -- co_scope       Logger scope
      -- co_invalid     Message code without expected value.
      -- co_invalid_exp Message code with expected value.
      co_scope       constant logger_logs.scope%type := co_scope_prefix || 'not_supported';
      co_invalid     constant utl_public_types.code_type := 'UTL_INVALID';
      co_invalid_exp constant utl_public_types.code_type := 'UTL_INVALID_EXP';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      if in_value_exp is null
      then
         utl_msg_api.set_name(in_msg_code => co_invalid);
         utl_msg_api.set_token(in_token_val => in_item);
         utl_msg_api.set_token(in_token_val => in_value);
      else
         utl_msg_api.set_name(in_msg_code => co_invalid_exp);
         utl_msg_api.set_token(in_token_val => in_item);
         utl_msg_api.set_token(in_token_val => in_value);
         utl_msg_api.set_token(in_token_val => in_value_exp);
      end if;
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_item', p_val => in_item);
         logger.append_param(p_params => lt_params, p_name => 'in_value', p_val => in_value);
         logger.append_param(p_params => lt_params, p_name => 'in_value_exp', p_val => in_value_exp);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end invalid;

   /**
   * Sets a generic I/O error message.
   *
   * @param in_scope      logger scope.
   * @param in_io_context [file path, fmode] or [clob, fmode].
   * @param in_file_error Error code.
   */
   function io_error(in_scope      in utl_public_types.logger_scope_type
                    ,in_io_context in utl_public_types.text_type
                    ,in_file_error in utl_public_types.code_type) return utl_public_types.text_type is
      --
      -- co_scope    Logger scope
      -- co_io_error Message code.
      co_scope    constant utl_public_types.logger_scope_type := co_scope_prefix || 'not_supported';
      co_io_error constant utl_public_types.code_type := 'UTL_IO_ERROR';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_io_error);
      utl_msg_api.set_token(in_token_val => in_scope);
      utl_msg_api.set_token(in_token_val => in_io_context);
      utl_msg_api.set_token(in_token_val => in_file_error);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_scope', p_val => in_scope);
         logger.append_param(p_params => lt_params, p_name => 'in_io_context', p_val => in_io_context);
         logger.append_param(p_params => lt_params, p_name => 'in_file_error', p_val => in_file_error);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
   end io_error;

   /**
   * Sets a template already defined error.
   *
   * @param in_entity Entity involved:
   *                  {*} TEMPLATE Text Template
   *                  {*) SCOPE    Scope 
   *                  {*} VARIABLE Variable
   * @param in_code   Entity instance involved (UK).
   */
   function tt_already_defined(in_entity in utl_public_types.code_type
                              ,in_code   in utl_public_types.code_type) return utl_msg_api.r_msg_info_type is
      --
      -- co_scope    Logger scope
      -- co_io_error Message code.
      co_scope              constant utl_public_types.logger_scope_type := co_scope_prefix || 'tt_already_defined';
      co_tt_already_defined constant utl_public_types.code_type := 'UTL_TT_ALREADY_DEFINED';
      co_tt_entities        constant utl_public_types.code_type := 'TT_ENTITIES';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_tt_already_defined);
      utl_msg_api.set_token(utl_lookups_api.meaning(in_lut_code => co_tt_entities, in_code => in_entity));
      utl_msg_api.set_token(in_code);
      --
      return utl_msg_api.get_encoded();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_entity', p_val => in_entity);
         logger.append_param(p_params => lt_params, p_name => 'in_code', p_val => in_code);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end tt_already_defined;

   /**
   * Sets a sub template not used error.
   *
   * @param in_lst List of sub templates  
   */
   function tt_not_used(in_lst in utl_public_types.code_type) return utl_public_types.text_type is
      --
      -- co_scope    Logger scope
      -- co_io_error Message code.
      co_scope       constant utl_public_types.logger_scope_type := co_scope_prefix || 'tt_not_used';
      co_tt_not_used constant utl_public_types.code_type := 'UTL_TT_NOT_USED';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_tt_not_used);
      utl_msg_api.set_token(in_lst);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_lst', p_val => in_lst);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end tt_not_used;

   /**
   * Sets a sub template not used error.
   *
   * @param in_lst List of sub templates  
   */
   function tt_not_defined(in_lst in utl_public_types.code_type) return utl_public_types.text_type is
      --
      -- co_scope    Logger scope
      -- co_io_error Message code.
      co_scope          constant utl_public_types.logger_scope_type := co_scope_prefix || 'tt_not_defined';
      co_tt_not_defined constant utl_public_types.code_type := 'UTL_TT_NOT_DEFINED';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_tt_not_defined);
      utl_msg_api.set_token(in_token_val => in_lst);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_lst', p_val => in_lst);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end tt_not_defined;

   /**
   * Sets a generic bind variable not set message.
   *
   * @param in_cursor_id UTL_SQL Cursor.
   * @param in_var_name  Bind variable name.
   */
   function bind_not_set(in_cursor_id utl_sql.cursor_id_type
                        ,in_var_name  in utl_public_types.long_code_type) return utl_public_types.text_type is
      --
      -- co_scope    Logger scope
      -- co_io_error Message code.
      co_scope        constant utl_public_types.logger_scope_type := co_scope_prefix || 'bind_not_set';
      co_bind_not_set constant utl_public_types.code_type := 'UTL_BIND_NOT_SET';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_bind_not_set);
      utl_msg_api.set_token(in_token_val => in_cursor_id);
      utl_msg_api.set_token(in_token_val => in_var_name);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_cursor_id', p_val => in_cursor_id);
         logger.append_param(p_params => lt_params, p_name => 'in_var_name', p_val => in_var_name);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end bind_not_set;

   /**
   * Sets a generic global variable not initialized error.
   *
   * @param in_global Global name.
   */
   function not_initialized(in_global in utl_public_types.object_name_type) return utl_public_types.text_type is
      --
      -- co_scope           Logger scope
      -- co_not_initialized Message code.
      co_scope           constant utl_public_types.logger_scope_type := co_scope_prefix || 'not_initialized';
      co_not_initialized constant utl_public_types.code_type := 'UTL_NOT_INITIALIZED';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_not_initialized);
      utl_msg_api.set_token(in_token_val => in_global);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_global', p_val => in_global);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end not_initialized;

   /**
   * Sets the INCONVERTABLE CHAR message.
   */
   function inconvertible_char return utl_public_types.text_type is
      --
      -- co_scope           Logger scope
      -- co_not_initialized Message code.
      co_scope              constant utl_public_types.logger_scope_type := co_scope_prefix || 'inconvertible_char';
      co_inconvertible_char constant utl_public_types.code_type := 'UTL_INCONVERTIBLE_CHAR';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_inconvertible_char);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end inconvertible_char;

   /**
   * Sets a generic bind variable not set message.
   *
   * %param in_cursor_id UTL_SQL Cursor.
   */
   function cursor_already_open(in_cursor_id utl_sql.cursor_id_type) return utl_public_types.text_type is
      --
      -- co_scope    Logger scope
      -- co_io_error Message code.
      co_scope               constant utl_public_types.logger_scope_type := co_scope_prefix || 'cursor_already_open';
      co_cursor_already_open constant utl_public_types.code_type := 'UTL_CURSOR_ALREADY_OPEN';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      utl_msg_api.set_name(in_msg_code => co_cursor_already_open);
      utl_msg_api.set_token(in_token_val => in_cursor_id);
      --
      return utl_msg_api.get();
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_cursor_id', p_val => in_cursor_id);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end cursor_already_open;

   /**
   * Returns the message text for a constraint error.
   *
   * @param in_msg_code Message code.
   *
   * %return The message text
   */
   function msg(in_msg_code in utl_public_types.code_type) return utl_public_types.text_type is
      --
      -- co_scope    Logger scope
      co_scope constant utl_public_types.logger_scope_type := co_scope_prefix || 'msg';
      --
      -- lt_params Logger parameters.
      -- l_msg     Mesaage to return.
      lt_params logger.tab_param;
      l_msg     utl_public_types.text_type;
   begin
      if in_msg_code is not null
      then
         utl_msg_api.set_name(in_msg_code => in_msg_code);
         l_msg := utl_msg_api.get();
      end if;
      --
      return l_msg;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_msg_code', p_val => in_msg_code);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end msg;

   /**
   * Returns the message text for a constraint error.
   *
   * @param in_msg_code Message code.
   * @param in_tokens         Tokens (optional).
   * @param in_item_name      Associated APEX item
   *
   * %return The encoded message info
   */
   function msg_info(in_msg_code  in utl_public_types.code_type
                    ,in_tokens    in apex_t_varchar2 default null
                    ,in_item_name in utl_public_types.apex_name_type default null) return utl_msg_api.r_msg_info_type is
      --
      -- co_scope    Logger scope
      co_scope constant utl_public_types.logger_scope_type := co_scope_prefix || 'msg_info';
      --
      -- lt_params Logger parameters.
      -- l_msg     Mesaage to return.
      lt_params logger.tab_param;
      l_msg     utl_msg_api.r_msg_info_type;
      l_idx     pls_integer;
   begin
      if in_msg_code is not null
      then
         utl_msg_api.set_name(in_msg_code => in_msg_code, in_item_name => in_item_name);
         if in_tokens is not null
         then
            l_idx := in_tokens.first;
            while l_idx is not null
            loop
               utl_msg_api.set_token(in_token_val => in_tokens(l_idx));
               l_idx := in_tokens.next(l_idx);
            end loop;
         end if;
         l_msg := utl_msg_api.get_encoded;
      end if;
      --
      return l_msg;
   exception
      when others then
         logger.append_param(p_params => lt_params, p_name => 'in_msg_code', p_val => in_msg_code);
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end msg_info;
   /**
   * Returns the Error Button text.
   *
   * %return The Error Button text 
   */
   function error_btn return utl_public_types.text_type deterministic is
      --
      -- co_scope    Logger scope
      co_scope constant utl_public_types.logger_scope_type := co_scope_prefix || 'error_btn';
      --
      -- lt_params Logger parameters.
      lt_params logger.tab_param;
   begin
      return msg(in_msg_code => 'UTL_ERROR_BTN');
   exception
      when others then
         logger.log_error(p_text => 'Unhandled Exception', p_scope => co_scope, p_params => lt_params);
         raise;
   end error_btn;
end utl_msg;
]]></n0:source>
		</n0:createOraclePackageBody>
	</changeSet>
</databaseChangeLog>
