<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog 
	xmlns="http://www.liquibase.org/xml/ns/dbchangelog" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:n0="http://www.oracle.com/xml/ns/dbchangelog-ext" 
	xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog 
	http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.9.xsd">
	<changeSet id="ce4fb82d1f4f559ba9f7b1c102ca08ca62822397" author="(UTL_API)-Generated" failOnError="false"    >
		<n0:createOraclePackageSpec objectName="UTL_IO" objectType="PACKAGE_SPEC" ownerName="UTL_API"   >
			<n0:source><![CDATA[
  CREATE OR REPLACE EDITIONABLE PACKAGE "UTL_API"."UTL_IO" is
   /**
   * Text file/CLOB reader/writer API.
   */

   /**
   * Default newline character
   */
   co_newline constant varchar2(2) := chr(10);
   /**
   * Clob Write mode.
   */
   co_cw constant varchar2(2) := 'CW';
   /**
   * Clob Read mode.
   */
   co_cr constant varchar2(2) := 'CR';
   /**
   * File Read mode.
   */
   co_fr constant varchar2(2) := 'FR';
   /**
   * File Write mode.
   */
   co_fw constant varchar2(2) := 'FW';
   /**
   * Bufferlength.
   */
   co_max_buf_len constant pls_integer := 500;

   /**
   * Returns the absolute OS file path for the Oracle directory and file name passed in.
   *
   * @param in_directory Oracle Directory to handle.
   * @param in_filename File name to handle.
   *
   * @return Absolute OS file path for the Oracle directory and file name passed in.
   */
   function get_path(in_directory utl_public_types.object_name_type
                    ,in_filename  utl_public_types.text_type) return utl_public_types.text_type;

   /**
   * Closes the given UTL_IO_OT instance.
   *
   * @param in_file_idx The UTL_IO_OT instance to close.
   */
   procedure close_file(in_file_idx in utl_public_types.idx_type);

   /**
   * Returns the CLOB value from the given UTL_CLOB_OT instance.
   *
   * @param in_file_idx The UTL_CLOB_OT instance to handle.
   *
   * @return {*} CLOBVAL The clob value for the given instance (UTL_CLOB_OT.CLOBVAL).
   *         {*} NULL    Something went wrong.
   */
   function getclob(in_file_idx in utl_public_types.idx_type) return clob;

   /**
   * Open a file for reading (Overloaded, OS text file).
   *
   * @param pc_dir OS directory (Oracle directory object) where the file is located.
   * @param pc_fnm Filename to open.
   *
   * @return Opened file idx.
   */
   function open_read(in_directory in utl_public_types.object_name_type
                     ,in_filename  in utl_public_types.text_type) return utl_public_types.idx_type;

   /**
   * Opens a clob for reading (Overloaded, CLOB).
   *
   * @param in_clobval CLOB to read from.
   *
   * @return Opened file idx.
   */
   function open_read(in_clobval in clob) return utl_public_types.idx_type;

   /**
   * Opens a OS text file for writing (Overloaded, OS text file).
   *
   * @param in_dir OS directory (Oracle directory object) where the file is located.
   * @param in_fnm Filename to open.
   *
   * @return Opened file idx.
   */
   function open_write(in_directory in utl_public_types.object_name_type
                      ,in_filename  in utl_public_types.text_type) return utl_public_types.idx_type;

   /**
   * Opens a clob for writing (Overloaded, CLOB).
   *
   * @param in_newline New line sequence to be used (LF or CRLF).
   *
   * @return Opened file idx.
   */
   function open_write(in_newline in utl_string.sep_type default null) return utl_public_types.idx_type;

   /**
   * Writes the text table IN_TEXT_TAB to the given UTL_IO_OT instance (Overloaded, UTL_COLLECTIONS.T_STR_TYPE).
   *
   * @param in_file_idx    File to handle.
   * @param in_text        Text table to write.
   * @param in_ind_newline Indicates whether a newline character must be written (1/0).
   */
   procedure put(in_file_idx    in utl_public_types.idx_type
                ,in_text        in utl_collections.t_str_type
                ,in_ind_newline in utl_public_types.ind_type default 1);

   /**
   * Writes IN_TEXT to the given UTL_IO_OT instance (Overloaded, varchar2).
   *
   * @param in_file_idx    File to handle.
   * @param in_text        Text to write.
   * @param in_ind_newline Indicates whether a newline character must be written (1/0).
   */
   procedure put(in_file_idx    in utl_public_types.idx_type
                ,in_text        in utl_public_types.str_type
                ,in_ind_newline in utl_public_types.ind_type default 1);

   /**
   * Writes IN_TEXT to the given UTL_IO_OT instance. (Overloaded, clob)
   *
   * @param in_file_idx    File to handle.
   * @param in_text        Text to write.
   * @param in_ind_newline Indicates whether a newline character must be written (1/0).
   */
   procedure put(in_file_idx    in utl_public_types.idx_type
                ,in_text        in clob
                ,in_ind_newline in utl_public_types.ind_type default 1);

   /**
   * Reads a line from the given UTL_IO_OT instance.
   *
   * @param in_file_idx File to handle.
   * @param out_line    Line read.
   * @param out_eof     EOF 
   *                    {*} TRUE  The end of the file/CLOB is reached.
   *                    {*} FALSE The end of the file/CLOB is not reached yet.
   */
   procedure get_line(in_file_idx in utl_public_types.idx_type
                     ,out_line    out nocopy utl_public_types.text_type
                     ,out_eof     out nocopy boolean);
end utl_io;
]]></n0:source>
		</n0:createOraclePackageSpec>
	</changeSet>
</databaseChangeLog>
