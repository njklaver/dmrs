create user "UTL_DATA" 
   identified by values 'S:6A6FA464087F62BF4ACBF941EF0E677BA325FECA03676FAB5F16F70646AA;T:89B366FE0AFA8FF466EFE28DDBEF89FBE6A80200581F69D27634D61848378D8C30F0648CB297224C133B720D2C98BA6F8766D985DE3F0628F57289619255559C73F09EB4540835C70103AFD6AF4B221D'
   default tablespace "DATA"
   temporary tablespace "TEMP"
   profile "APEX_PUBLIC_USER";

alter user "UTL_DATA" enable EDITIONS for view, synonym, procedure, function, package, trigger, type, library, sql TRANSLATION profile;

grant "CONNECT" to "UTL_DATA";
grant "PLSQL_DYN_PERFORMANCE" to "UTL_DATA";

alter user "UTL_DATA" default role all;

grant create session to "UTL_DATA";
grant create table to "UTL_DATA";
grant create cluster to "UTL_DATA";
grant create synonym to "UTL_DATA";
grant create view to "UTL_DATA";
grant create sequence to "UTL_DATA";
grant create procedure to "UTL_DATA";
grant create trigger to "UTL_DATA";
grant create materialized view to "UTL_DATA";
grant create type to "UTL_DATA";
grant create operator to "UTL_DATA";
grant create indextype to "UTL_DATA";
grant create dimension to "UTL_DATA";
grant create job to "UTL_DATA";

declare
   temp_count number;
   sqlstr     varchar2(200);
begin
   sqlstr := 'alter user "UTL_DATA" quota unlimited on "DATA"';
   execute immediate sqlstr;
exception
   when others then
      if sqlcode = -30041
      then
         sqlstr := 'select count(*) from user_tablespaces where tablespace_name = ''DATA'' and contents = ''TEMPORARY''';
         execute immediate sqlstr
            into temp_count;
         if temp_count = 1
         then
            return;
         else
            raise;
         end if;
      else
         raise;
      end if;
end;
/

