create user DMRS_API 
   identified by values 'S:7D48B714708EFB5093891B7567E50C3F91E207EFB1F8196A54144BED004D;T:2AA4E4D722BFE87999E23F1E88B92D61BF81116D61026E7B9CD3FB395821C53273789A8CF702207D3DC7C690ECAC47D631E4813DA785084B6751F8F8F7C1534E6FA26344C4D8E00972D4ADECA34F782F'
   default tablespace data
   temporary tablespace TEMP;

grant connect to dmrs_api;
grant aq_administrator_role to dmrs_api;
grant aq_user_role to dmrs_api;
grant plsql_dyn_performance to dmrs_api;

alter user dmrs_api default role all;

grant create session to dmrs_api;
grant create table to dmrs_api;
grant create any table to dmrs_api;
grant alter any table to dmrs_api;
grant create cluster to dmrs_api;
grant create synonym to dmrs_api;
grant create view to dmrs_api;
grant create sequence to dmrs_api;
grant create procedure to dmrs_api;
grant create trigger to dmrs_api;
grant create any trigger to dmrs_api;
grant alter any trigger to dmrs_api;
grant create materialized view to dmrs_api;
grant create any directory to dmrs_api;
grant create type to dmrs_api;
grant create operator to dmrs_api;
grant create indextype to dmrs_api;
grant create dimension to dmrs_api;
grant create any context to dmrs_api;
grant debug connect session to dmrs_api;
grant debug connect any to dmrs_api;
grant debug any procedure to dmrs_api;
grant create job to dmrs_api;
grant execute dynamic mle to dmrs_api;
