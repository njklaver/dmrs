create user DM_REPOS 
   identified by values 'S:E8C423621E6FEB40242A36687BFF61DAAEBB4A1AB74EC042A94E52443F03;T:F374EC85E2D003ABCC2A70489CA9DC784BF5B167A5DBB4D4F17CED7B883E9778F5E908792AA4D4D90C3A4935541EABDAF1F07E4A114E88259C3BACB1D964200E24B04E972BD4AA7CC51CF4D00B351CB3'
   default tablespace data
   temporary tablespace temp
   profile apex_public_user;

grant create session to dm_repos;
grant create table to dm_repos;
grant create cluster to dm_repos;
grant create view to dm_repos;
grant create sequence to dm_repos;
grant create procedure to dm_repos;
grant create trigger to dm_repos;
grant create type to dm_repos;

declare
   temp_count number;
   sqlstr     varchar2(200);
begin
   sqlstr := 'alter user dm_repos quota unlimited on DATA';
   execute immediate sqlstr;
exception
   when others then
      if sqlcode = -30041
      then
         sqlstr := 'select count(*) from user_tablespaces where tablespace_name = ''DATA'' and contents = ''TEMPORARY''';
         execute immediate sqlstr
            into temp_count;
         if temp_count = 1
         then
            return;
         else
            raise;
         end if;
      else
         raise;
      end if;
end;
/
