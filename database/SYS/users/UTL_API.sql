create user utl_api 
   identified by values 'S:07A2513E00D1E03A905330954A21FFFCDD6140EAA4E19EFEF2A37E8116BA;T:E7517D9EFC146A44048F3D5E4138C2F47505ABDE4D8CA450703FC670C9C3938F5891E72DB25B73437A3785821889EDEA651AC025931D1038CCA7B0F3097B6AC291F07AE629034BFCAC66ED01B87025AE'
   default tablespace data
   temporary tablespace temp;
alter user utl_api enable editions for view, synonym, procedure, function, package, trigger, type, library, sql translation profile;

grant connect to utl_api;
grant aq_administrator_role to utl_api;
grant aq_user_role to utl_api;
grant plsql_dyn_performance to utl_api;

alter user utl_api default role all;

grant create session to utl_api;
grant create any table to utl_api;
grant alter any table to utl_api;
grant create cluster to utl_api;
grant create synonym to utl_api;
grant create view to utl_api;
grant create sequence to utl_api;
grant create procedure to utl_api;
grant create any trigger to utl_api;
grant alter any trigger to utl_api;
grant create materialized view to utl_api;
grant create any directory to utl_api;
grant create type to utl_api;
grant create operator to utl_api;
grant create indextype to utl_api;
grant create dimension to utl_api;
grant create any context to utl_api;
grant debug connect session to utl_api;
grant debug connect any to utl_api;
grant debug any procedure to utl_api;
grant create job to utl_api;

grant execute on sys.dbms_session to utl_api;
grant select on sys.dba_scheduler_job_classes to utl_api;
grant execute on sys.dbms_crypto to utl_api;
grant execute on sys.dbms_aq to utl_api;
