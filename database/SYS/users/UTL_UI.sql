create user utl_ui 
   identified by values 'S:DA21A7B5BF835FD8336866F4EEAB6EF6E8B7BBDADCB927467A43E1D97B8E;T:05A218D352AD674C0C7CF875027DDBC9CE03EA6AC29A113C3704BCA2E7A3E387D5B694FE93245721B94F68C9821927185B6385E15E31EB57C2C6F091A90D8D553D6C7F7343F8C702807F244117001E76'
   default tablespace data
   temporary tablespace temp;
alter user utl_ui enable editions for view, synonym, procedure, function, package, trigger, type, library, sql translation profile;

grant create cluster to utl_ui;
grant create synonym to utl_ui;
grant create sequence to utl_ui;
grant create procedure to utl_ui;
grant create trigger to utl_ui;
grant create materialized view to utl_ui;
grant create type to utl_ui;
grant create operator to utl_ui;
grant create indextype to utl_ui;
grant create dimension to utl_ui;
grant create job to utl_ui;
grant create session to utl_ui;
