prompt --application/pages/page_00003
begin
--   Manifest
--     PAGE: 00003
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.10.15'
,p_release=>'21.2.1'
,p_default_workspace_id=>2700555835947928
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'UTL_UI'
);
wwv_flow_api.create_page(
 p_id=>3
,p_user_interface_id=>wwv_flow_api.id(3094053265415894)
,p_name=>'Setup'
,p_alias=>'SETUP'
,p_step_title=>'Setup'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'UTL_ADMIN'
,p_last_upd_yyyymmddhh24miss=>'20211221125741'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3501013035685223)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#:t-BreadcrumbRegion--useBreadcrumbTitle'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(3008806992415841)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(2905933128415793)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(3070987320415869)
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3615017854075227)
,p_plug_name=>'Setup'
,p_icon_css_classes=>'fa-gears'
,p_region_template_options=>'t-Region--showIcon:t-Region--scrollBody'
,p_component_template_options=>'t-MediaList--showIcons:t-MediaList--showDesc:u-colors:t-MediaList--cols t-MediaList--5cols'
,p_plug_template=>wwv_flow_api.id(2996765578415838)
,p_plug_display_sequence=>20
,p_list_id=>wwv_flow_api.id(3614497411075226)
,p_plug_source_type=>'NATIVE_LIST'
,p_list_template_id=>wwv_flow_api.id(3051101986415859)
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.component_end;
end;
/
