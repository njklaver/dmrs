prompt --application/shared_components/navigation/lists/dml_events
begin
--   Manifest
--     LIST: DML Events
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.10.15'
,p_release=>'21.2.1'
,p_default_workspace_id=>2700555835947928
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'UTL_UI'
);
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(4857546383179380)
,p_name=>'DML Events'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(4857781606179381)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'DML event types'
,p_list_item_link_target=>'f?p=&APP_ID.:7:&SESSION.::&DEBUG.:7:::'
,p_list_item_icon=>'fa-table-clock'
,p_list_text_01=>'DML Event type definition'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.component_end;
end;
/
