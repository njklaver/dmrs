prompt --application/shared_components/navigation/lists/setup
begin
--   Manifest
--     LIST: Setup
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.10.15'
,p_release=>'21.2.1'
,p_default_workspace_id=>2700555835947928
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'UTL_UI'
);
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(3614497411075226)
,p_name=>'Setup'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(3614636728075227)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Lookup types'
,p_list_item_link_target=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.:4:::'
,p_list_item_icon=>'fa-table-search'
,p_list_text_01=>'Reference data'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.component_end;
end;
/
