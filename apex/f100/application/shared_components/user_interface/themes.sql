prompt --application/shared_components/user_interface/themes
begin
--   Manifest
--     THEME: 100
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.10.15'
,p_release=>'21.2.1'
,p_default_workspace_id=>2700555835947928
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'UTL_UI'
);
wwv_flow_api.create_theme(
 p_id=>wwv_flow_api.id(3072358453415879)
,p_theme_id=>42
,p_theme_name=>'Universal Theme'
,p_theme_internal_name=>'UNIVERSAL_THEME'
,p_ui_type_name=>'DESKTOP'
,p_navigation_type=>'L'
,p_nav_bar_type=>'LIST'
,p_reference_id=>4070917134413059350
,p_is_locked=>false
,p_default_page_template=>wwv_flow_api.id(2917350880415807)
,p_default_dialog_template=>wwv_flow_api.id(2935645008415813)
,p_error_template=>wwv_flow_api.id(2933376775415812)
,p_printer_friendly_template=>wwv_flow_api.id(2917350880415807)
,p_breadcrumb_display_point=>'REGION_POSITION_01'
,p_sidebar_display_point=>'REGION_POSITION_02'
,p_login_template=>wwv_flow_api.id(2933376775415812)
,p_default_button_template=>wwv_flow_api.id(3069309841415868)
,p_default_region_template=>wwv_flow_api.id(2996765578415838)
,p_default_chart_template=>wwv_flow_api.id(2996765578415838)
,p_default_form_template=>wwv_flow_api.id(2996765578415838)
,p_default_reportr_template=>wwv_flow_api.id(2996765578415838)
,p_default_tabform_template=>wwv_flow_api.id(2996765578415838)
,p_default_wizard_template=>wwv_flow_api.id(2996765578415838)
,p_default_menur_template=>wwv_flow_api.id(3008806992415841)
,p_default_listr_template=>wwv_flow_api.id(2996765578415838)
,p_default_irr_template=>wwv_flow_api.id(2992469042415836)
,p_default_report_template=>wwv_flow_api.id(3031739604415850)
,p_default_label_template=>wwv_flow_api.id(3066882587415866)
,p_default_menu_template=>wwv_flow_api.id(3070987320415869)
,p_default_calendar_template=>wwv_flow_api.id(3071068004415870)
,p_default_list_template=>wwv_flow_api.id(3064977546415864)
,p_default_nav_list_template=>wwv_flow_api.id(3055945511415861)
,p_default_top_nav_list_temp=>wwv_flow_api.id(3055945511415861)
,p_default_side_nav_list_temp=>wwv_flow_api.id(3054145875415860)
,p_default_nav_list_position=>'SIDE'
,p_default_dialogbtnr_template=>wwv_flow_api.id(2945100098415819)
,p_default_dialogr_template=>wwv_flow_api.id(2942310448415818)
,p_default_option_label=>wwv_flow_api.id(3066882587415866)
,p_default_required_label=>wwv_flow_api.id(3068129843415866)
,p_default_page_transition=>'NONE'
,p_default_popup_transition=>'NONE'
,p_default_navbar_list_template=>wwv_flow_api.id(3056989177415862)
,p_file_prefix => nvl(wwv_flow_application_install.get_static_theme_file_prefix(42),'#IMAGE_PREFIX#themes/theme_42/21.2/')
,p_files_version=>64
,p_icon_library=>'FONTAPEX'
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#libraries/apex/#MIN_DIRECTORY#widget.stickyWidget#MIN#.js?v=#APEX_VERSION#',
'#THEME_IMAGES#js/theme42#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>'#THEME_IMAGES#css/Core#MIN#.css?v=#APEX_VERSION#'
);
wwv_flow_api.component_end;
end;
/
