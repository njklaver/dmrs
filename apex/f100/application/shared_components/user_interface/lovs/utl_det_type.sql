prompt --application/shared_components/user_interface/lovs/utl_det_type
begin
--   Manifest
--     UTL_DET_TYPE
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.10.15'
,p_release=>'21.2.1'
,p_default_workspace_id=>2700555835947928
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'UTL_UI'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(4829646775396593)
,p_lov_name=>'UTL_DET_TYPE'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select lup.meaning as d',
'      ,lup.code    as r',
'from   utl_g_lookups_v lup',
'where  lup.lut_code = ''UTL_DET_TYPE''',
''))
,p_source_type=>'SQL'
,p_location=>'LOCAL'
,p_use_local_sync_table=>false
,p_query_owner=>'UTL_API'
,p_return_column_name=>'R'
,p_display_column_name=>'D'
,p_group_sort_direction=>'ASC'
,p_default_sort_column_name=>'D'
,p_default_sort_direction=>'ASC_NULLS_FIRST'
);
wwv_flow_api.component_end;
end;
/
