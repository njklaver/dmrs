prompt --application/shared_components/user_interface/lovs/utl_master_operation
begin
--   Manifest
--     UTL_MASTER_OPERATION
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.10.15'
,p_release=>'21.2.1'
,p_default_workspace_id=>2700555835947928
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'UTL_UI'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(4832070366537115)
,p_lov_name=>'UTL_MASTER_OPERATION'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select lup.meaning as d',
'      ,lup.code    as r',
'from   utl_g_lookups_v lup',
'where  lup.lut_code = ''UTL_MASTER_OPERATION''',
''))
,p_source_type=>'SQL'
,p_location=>'LOCAL'
,p_query_owner=>'UTL_API'
,p_return_column_name=>'R'
,p_display_column_name=>'D'
,p_default_sort_column_name=>'D'
,p_default_sort_direction=>'ASC'
);
wwv_flow_api.component_end;
end;
/
