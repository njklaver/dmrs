prompt --application/shared_components/user_interface/lovs/utl_yes_no
begin
--   Manifest
--     UTL_YES_NO
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.10.15'
,p_release=>'21.2.1'
,p_default_workspace_id=>2700555835947928
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'UTL_UI'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(4828272342277497)
,p_lov_name=>'UTL_YES_NO'
,p_source_type=>'TABLE'
,p_location=>'LOCAL'
,p_use_local_sync_table=>false
,p_query_owner=>'UTL_API'
,p_query_table=>'UTL_G_IND_YES_NO'
,p_return_column_name=>'CODE'
,p_display_column_name=>'MEANING'
,p_group_sort_direction=>'ASC'
,p_default_sort_column_name=>'MEANING'
,p_default_sort_direction=>'ASC'
);
wwv_flow_api.component_end;
end;
/
