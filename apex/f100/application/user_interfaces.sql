prompt --application/user_interfaces
begin
--   Manifest
--     USER INTERFACES: 100
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.10.15'
,p_release=>'21.2.1'
,p_default_workspace_id=>2700555835947928
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'UTL_UI'
);
wwv_flow_api.create_user_interface(
 p_id=>wwv_flow_api.id(3094053265415894)
,p_ui_type_name=>'DESKTOP'
,p_display_name=>'Desktop'
,p_display_seq=>10
,p_use_auto_detect=>false
,p_is_default=>true
,p_theme_id=>42
,p_home_url=>'f?p=&APP_ID.:1:&SESSION.'
,p_login_url=>'f?p=&APP_ID.:LOGIN:&APP_SESSION.::&DEBUG.:::'
,p_theme_style_by_user_pref=>false
,p_built_with_love=>false
,p_global_page_id=>0
,p_navigation_list_id=>wwv_flow_api.id(2906478425415795)
,p_navigation_list_position=>'TOP'
,p_navigation_list_template_id=>wwv_flow_api.id(3042789503415855)
,p_nav_list_template_options=>'#DEFAULT#:t-MegaMenu--layout5Cols'
,p_css_file_urls=>'#APP_IMAGES#utl#MIN#.css'
,p_javascript_file_urls=>'#APP_IMAGES#utl#MIN#.js'
,p_nav_bar_type=>'LIST'
,p_nav_bar_list_id=>wwv_flow_api.id(3093747407415893)
,p_nav_bar_list_template_id=>wwv_flow_api.id(3056989177415862)
,p_nav_bar_template_options=>'#DEFAULT#'
);
wwv_flow_api.component_end;
end;
/
