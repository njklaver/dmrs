BEGIN
  sys.dbms_aqadm.create_queue(queue_name     => 'UTL_EVENT_QUEUE',
                              queue_table    => 'UTL_EVENT_QUEUE_TAB',
                              queue_type     => sys.dbms_aqadm.normal_queue,
                              max_retries    => 5,
                              retry_delay    => 0,
                              retention_time => 0);
END;
/
