BEGIN
   utl_db_context.init_script(in_application_id => 111, in_user_id => 'UTL_UI');
   --
   DELETE FROM utl_message_masters
   WHERE  id = -1;

   INSERT INTO utl_data.utl_message_masters
      (master_type
      ,master_pk
      ,id
      ,created 
      ,created_by )
   VALUES
      ('$'
      ,-1
      ,-1
      ,LOCALTIMESTAMP
      ,utl_db_context.app_user);

   INSERT INTO utl_messages
      (id
      ,created
      ,created_by
      ,msg_text
      ,mas_id)
   VALUES
      (-1
      ,localtimestamp
      ,utl_db_context.app_user
      ,'$'
      ,-1);

   FOR idx IN 1 .. utl_error.co_max_context
   LOOP
      INSERT INTO utl_message_context
         (msg_ctx_code
         ,msg_ctx_value
         ,msg_id
         ,msg_ctx_idx)
      VALUES
         ('$' || ltrim(to_char(idx, '00'))
         ,'$' || ltrim(to_char(idx, '00'))
         ,-1
         ,utl_error.co_max_context + idx);
   END LOOP;

   COMMIT;
END;
/
