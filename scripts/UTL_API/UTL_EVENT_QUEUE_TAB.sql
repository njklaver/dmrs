BEGIN
  sys.dbms_aqadm.create_queue_table(queue_table        => 'UTL_EVENT_QUEUE_TAB',
                                    queue_payload_type => 'UTL_API.UTL_EVENT_OT',
                                    sort_list          => 'ENQ_TIME',
                                    multiple_consumers => TRUE,
                                    compatible         => '10.0.0',
                                    primary_instance   => 0,
                                    secondary_instance => 0,
                                    storage_clause     => 'tablespace UTL pctfree 10 initrans 1 maxtrans 255 storage ( initial 64K next 1M minextents 1 maxextents unlimited )');
END;
/
