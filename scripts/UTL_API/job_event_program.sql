BEGIN
  dbms_scheduler.create_program(program_name        => 'UTL_JOB_EVENT_LISTENER',
                                program_type        => 'STORED_PROCEDURE',
                                program_action      => 'utl_job_request_manager.job_event',
                                number_of_arguments => 1,
                                enabled             => FALSE);
  dbms_scheduler.define_metadata_argument(program_name       => 'UTL_JOB_EVENT_LISTENER',
                                          metadata_attribute => 'event_message',
                                          argument_position  => 1);
  dbms_scheduler.enable('UTL_JOB_EVENT_LISTENER');
END;
/
