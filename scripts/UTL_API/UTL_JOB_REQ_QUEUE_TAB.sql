BEGIN
  sys.dbms_aqadm.create_queue_table(queue_table        => 'UTL_JOB_REQ_QUEUE_TAB',
                                    queue_payload_type => 'UTL_API.UTL_JOB_REQUEST_OT',
                                    sort_list          => 'ENQ_TIME',
                                    compatible         => '10.0.0',
                                    primary_instance   => 0,
                                    secondary_instance => 0,
                                    storage_clause     => 'tablespace UTL pctfree 10 initrans 1 maxtrans 255 storage ( initial 64K next 1M minextents 1 maxextents unlimited )');
END;
/
