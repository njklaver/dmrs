CREATE OR REPLACE TYPE UTL_API.utl_file_ot force UNDER utl_io_ot
(
/**
  * This object type supports reading, creating of or writing to an OS text file.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */
/**
  * File id (UTL_FILE.FILE_TYPE.ID).
  */
  file_id INTEGER,
/**
  * File datatype (UTL_FILE.DATATYPE).
  */
  datatype INTEGER,
/**
  * OS (Database server) file path.
  */
  path VARCHAR2(4000),
/**
  * Creates a new UTL_FILE_OT instance.
  *
  * %param in_dir       OS directory (Oracle directory object) where the file must be created/opened.
  * %param in_fnm       Filename to create.
  * %param in_open_mode Specifies how the file is opened. Modes include:
  *                     {*} r read text
  *                     {*} w write text
  */
  CONSTRUCTOR FUNCTION utl_file_ot(in_dir       IN VARCHAR2,
                                   in_fnm       IN VARCHAR2,
                                   in_open_mode IN VARCHAR2) RETURN SELF AS RESULT,

/**
  * Closes this instance.
  */
  OVERRIDING MEMBER PROCEDURE close_file(SELF IN OUT utl_file_ot),

/**
  * Returns the OS path for this instance.
  *
  * %return The OS path for this instance.
  */
  OVERRIDING MEMBER FUNCTION getpath(SELF IN utl_file_ot) RETURN VARCHAR2,

/**
  * Writes IN_STR to this file.
  *
  * %param in_str         String to write.
  * %param in_newline_flg Indicates whether a newline character must be apended (Y/N).
  */
  OVERRIDING MEMBER PROCEDURE put(SELF           IN OUT utl_file_ot,
                                  in_str         IN VARCHAR2,
                                  in_newline_flg IN VARCHAR2),

/**
  * Reads the buffer from this file.
  */
  OVERRIDING MEMBER PROCEDURE read_buffer(SELF IN OUT utl_file_ot)
)
/

