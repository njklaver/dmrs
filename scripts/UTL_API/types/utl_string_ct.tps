CREATE OR REPLACE TYPE UTL_API.utl_string_ct force
  /**
  * UTL_PUBLIC_TYPES.STR_TYPE (VARCHAR2(32767)) spliited in chunks of 4000 characters.
  *
  * %author  Nico J. Klaver
  */
IS VARRAY(9) OF VARCHAR2(4000);
/

