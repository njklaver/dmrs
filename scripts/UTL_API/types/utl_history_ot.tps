CREATE OR REPLACE TYPE UTL_API.utl_history_ot FORCE AS OBJECT
(
  /**
  * Record history
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Table name
  */  
  table_name VARCHAR2(256),
  /**
  * PK.
  */
  pk NUMBER,
  /**
  * Label
  */
  label VARCHAR2(60),
  /**
  * Value.
  */
  value VARCHAR2(4000),
  /**
  * Sequence number.
  */
  seqnr INTEGER
)
/

