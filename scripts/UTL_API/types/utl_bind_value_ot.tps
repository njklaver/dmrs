create or replace type utl_api.utl_bind_value_ot force as object
(
  /**
  * UTL bind variable name/value pair.
  * 
  * %author Nico Klaver -  nklaver@itium.nl
  */
  /**
  * Bind variable name.
  */
  var_name varchar2(60),

  /**
  * Sets the bind variable value for the cursor supplied.
  *
  * %param in_cursor_id    UTL_SQL cursor id.
  */
  member procedure set_value(self            in utl_bind_value_ot,
                             in_cursor_id    in VARCHAR2),

  /**
  * Returns the bind variable value (VARCHAR2).
  *
  * %return Bind variable value.
  */
  member function get_c_value(self in utl_bind_value_ot) return varchar2,

  /**
  * Returns the bind variable value (NUMBER).
  *
  * %return Bind variable value.
  */
  member function get_n_value(self in utl_bind_value_ot) return number,

  /**
  * Returns the bind variable value (DATE).
  *
  * %return Bind variable value.
  */
  member function get_d_value(self in utl_bind_value_ot) return date,

  /**
  * Returns the bind variable value (NUMBER ARRAY).
  *
  * %return The bind variable value.
  */
  member function get_ntab_value(self in utl_bind_value_ot) return apex_t_number,

  /**
  * Returns the string value.
  *
  * %return The bind variable value converted to VARCHAR2.
  */
  member function get_value(self in utl_bind_value_ot) return varchar2,

  /**
  * Returns the bind variable name.
  *
  * %return Bind variable name.
  */
  member function get_var_name(self in utl_bind_value_ot) return varchar2
)
not final not instantiable
/

