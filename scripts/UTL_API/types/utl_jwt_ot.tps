CREATE OR REPLACE TYPE UTL_API.utl_jwt_ot force AS OBJECT
(
  /**
  * JSON Web Token
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  *  Claims.
  */
  jwt_claims utl_jwt_claim_ct,

  /**
  * Status
  */
  return_status VARCHAR2(1),

  /**
  * Creates a new UTL_JWT_OT instance by decoding the given JWT string.
  *
  * %param in_jwt JWT to decode.
  *
  * %return A new UTL_JWT_OT instance.
  */
  CONSTRUCTOR FUNCTION utl_jwt_ot(in_jwt IN VARCHAR2) RETURN SELF AS RESULT,

  /**
  * Creates a new UTL_JWT_OT instance.
  *
  * %param in_iss Issuer.
  * %param in_iat Issued at (POSIX-date).
  * %param in_jti Unique identifier.
  * %param in_exp Expired at (POSIX-date).
  *
  * %return A new utl_jwt_ot instance.
  */
  CONSTRUCTOR FUNCTION utl_jwt_ot(in_iss IN VARCHAR2,
                                  in_iat IN NUMBER,
                                  in_jti IN VARCHAR2,
                                  in_exp IN NUMBER DEFAULT NULL) RETURN SELF AS RESULT,

  /**
  * Sets a JWT claim.
  *
  * %param in_claim_name   Claim name.
  * %param in_claim_value  Claim value.
  * %param in_null   Always set the claim even when the value is null.
  */
  MEMBER PROCEDURE set_claim(SELF           IN OUT utl_jwt_ot,
                             in_claim_name  IN VARCHAR2,
                             in_claim_value IN VARCHAR2,
                             in_null        IN BOOLEAN DEFAULT FALSE),

  /**
  * Sets a Json Web Token claim.
  *
  * %param in_claim_name   Claim name.
  * %param in_claim_value  Claim value.
  * %param in_null   Always set the claim even when the value is null.
  */
  MEMBER PROCEDURE set_claim(SELF           IN OUT utl_jwt_ot,
                             in_claim_name  IN VARCHAR2,
                             in_claim_value IN NUMBER,
                             in_null        IN BOOLEAN DEFAULT FALSE),

  /**
  * Sets a JWT claim.
  *
  * %param in_claim_name   Claim name.
  * %param in_claim_value  Claim value.
  * %param in_null   Always set the claim even when the value is null.
  */
  MEMBER PROCEDURE set_claim(SELF IN OUT utl_jwt_ot,
                             in_claim_name  IN VARCHAR2,
                             in_claim_value IN DATE,
                             in_null        IN BOOLEAN DEFAULT FALSE),

  /**
  * Returns the JWT for this instance.
  *
  * %return The JWT.
  */
  MEMBER FUNCTION get_jwt(SELF IN utl_jwt_ot) RETURN VARCHAR2,

  /**
  * Returns the value for the claim given (NUMBER).
  *
  * %param in_claim_name  The claim to handle.
  * %param in_must_exist  Indicates whether the claim must exist.
  *
  * %return The claim value (NUMBER). 
  */
  MEMBER FUNCTION get_claim_n(SELF          IN utl_jwt_ot,
                              in_claim_name IN VARCHAR2,
                              in_must_exist IN BOOLEAN DEFAULT TRUE) RETURN NUMBER,

  /**
  * Returns the value for the claim given (VARCHAR2).
  *
  * %param in_claim_name The claim to handle.
  * %param in_must_exist Indicates whether the claim must exist.
  *
  * %return The claim value (VARCHAR2). 
  */
  MEMBER FUNCTION get_claim_c(SELF          IN utl_jwt_ot,
                              in_claim_name IN VARCHAR2,
                              in_must_exist IN BOOLEAN DEFAULT TRUE) RETURN VARCHAR2,

  /**
  * Returns the value for the claim given (DATE).
  *
  * %param in_claim_name  The claim to handle.
  * %param in_must_exist  Indicates whether the claim must exist.
  *
  * %return The claim value (DATE). 
  */
  MEMBER FUNCTION get_claim_d(SELF IN utl_jwt_ot,
                              
                              in_claim_name IN VARCHAR2,
                              in_must_exist IN BOOLEAN DEFAULT TRUE) RETURN DATE,

  /**
  * Checks whether this token is expired.
  *
  * %return {*} Y The token is expired
  *         {*} N The token is not expired.
  */
  MEMBER FUNCTION is_expired(SELF IN utl_jwt_ot) RETURN VARCHAR2
)
/

