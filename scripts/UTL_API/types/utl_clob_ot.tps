CREATE OR REPLACE TYPE UTL_API.utl_clob_ot force UNDER utl_io_ot
(
/**
  * This object type supports creating of, writing to or reading from a clob
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */
/**
  * CLOB contents.
  */
  clobval CLOB,
/**
  * New line character.
  */
  newline VARCHAR2(2),
/**
  * Position in <code>ac_clobval</code>.
  */
  pos NUMBER,
/**
  * Creates an empty instance for writing.
  *
  * %param in_newline New line sequence to be used (LF or CRLF).
  */
  CONSTRUCTOR FUNCTION utl_clob_ot(in_newline IN VARCHAR2 DEFAULT NULL) RETURN SELF AS RESULT,

/**
  * Creates an instance for reading.
  *
  * in_clobval clob to read.
  */
  CONSTRUCTOR FUNCTION utl_clob_ot(in_clobval IN CLOB) RETURN SELF AS RESULT,

/**
  * Closes this instance.
  */
  OVERRIDING MEMBER PROCEDURE close_file(SELF IN OUT utl_clob_ot),

/**
  * Returns CLOBVAL.
  *
  * %return CLOBVAL
  */
  OVERRIDING MEMBER FUNCTION getclob(SELF IN utl_clob_ot) RETURN CLOB,

/**
  * Writes IN_STR to CLOBVAL.
  *
  * %param in_str         String to write.
  * %param in_newline_flg Indicates whether a newline character must be apended (Y/N).
  */
  OVERRIDING MEMBER PROCEDURE put(SELF           IN OUT utl_clob_ot,
                                  in_str         IN VARCHAR2,
                                  in_newline_flg IN VARCHAR2),

/**
  * Reads the buffer from this instance.
  */
  OVERRIDING MEMBER PROCEDURE read_buffer(SELF IN OUT utl_clob_ot)
)
/

