CREATE OR REPLACE TYPE BODY UTL_API.utl_tt_row_ot IS
  /*
  * Text Template scope row
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  CONSTRUCTOR FUNCTION utl_tt_row_ot RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
  BEGIN
    self.var_idx := 0;
    self.t_vars  := utl_tt_var_ct();
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_tt_row_ot;

  CONSTRUCTOR FUNCTION utl_tt_row_ot(in_tt_code IN VARCHAR2) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
  BEGIN
    self.tt_code := in_tt_code;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_tt_row_ot;

  MEMBER PROCEDURE add_var(SELF     IN OUT NOCOPY utl_tt_row_ot,
                           in_name  IN VARCHAR2,
                           in_value IN VARCHAR2) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'add_var';
  BEGIN
    self.var_idx := self.var_idx + 1;
    self.t_vars.extend(1);
    self.t_vars(var_idx) := utl_tt_var_ot(in_name => in_name, in_value => in_value);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END add_var;

  /**
  * Generates this row.
  */
  MEMBER PROCEDURE generate(SELF                IN utl_tt_row_ot,
                            in_file_idx         IN PLS_INTEGER,
                            in_templ_text       IN VARCHAR2,
                            in_boilerplate_text IN CLOB,
                            in_newline_flg      IN VARCHAR2) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'generate';
    --
    -- t_vars All variabels name/value pairs.
    t_vars_idx utl_collections.t_str_str_type;
  BEGIN
    IF self.var_idx IS NOT NULL
    THEN
      IF in_templ_text IS NOT NULL
      THEN
        t_vars_idx := utl_text_template_util.var_idx(in_tt_row => SELF);
        utl_text_template_util.fill_in(in_file_idx    => in_file_idx,
                                       in_templ_text  => in_templ_text,
                                       in_vars_idx    => t_vars_idx,
                                       in_newline_flg => in_newline_flg);
      ELSE
        utl_io.put(in_file_idx => in_file_idx, in_text => in_boilerplate_text, in_newline_flg => in_newline_flg);
      END IF;
    ELSE
      utl_text_template_util.call_sub(in_templ_text => in_templ_text, in_file_idx => in_file_idx, in_sub_tt_code => self.tt_code);
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END generate;
END;
/

