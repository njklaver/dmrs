CREATE OR REPLACE TYPE BODY UTL_API.utl_jwt_claim_ot IS
  /*
  * JWT Claim
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Creates a new JWT Claim instance (VARCHAR2).
  *
  * %param in_claim_name  Claim name.
  * %param in_claim_value Claim value.
  */
  CONSTRUCTOR FUNCTION utl_jwt_claim_ot(in_claim_name  IN VARCHAR2,
                                        in_claim_value IN VARCHAR2) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_claim_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_jwt_claim_ot';
  BEGIN
    self.claim_name    := in_claim_name;
    self.claim_c_value := in_claim_value;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_jwt_claim_ot;
  /**
  * Creates a new JWT Claim instance (NUMBER).
  *
  * %param in_claim_name  Claim name.
  * %param in_claim_value Claim value.
  */
  CONSTRUCTOR FUNCTION utl_jwt_claim_ot(in_claim_name  IN VARCHAR2,
                                        in_claim_value IN NUMBER) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_claim_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_jwt_claim_ot';
  BEGIN
    self.claim_name    := in_claim_name;
    self.claim_n_value := in_claim_value;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_jwt_claim_ot;
  /**
  * Creates a new JWT Claim instance (DATE).
  *
  * %param in_claim_name  Claim name.
  * %param in_claim_value Claim value.
  */
  CONSTRUCTOR FUNCTION utl_jwt_claim_ot(in_claim_name  IN VARCHAR2,
                                        in_claim_value IN DATE) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_claim_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_jwt_claim_ot';
  BEGIN
    self.claim_name    := in_claim_name;
    self.claim_d_value := in_claim_value;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_jwt_claim_ot;
  /**
  * Adds this claim to the JWT.
  */
  MEMBER PROCEDURE json_str(SELF IN utl_jwt_claim_ot) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_claim_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'json_str';
  BEGIN
    IF self.claim_c_value IS NOT NULL
    THEN
      apex_json.write(p_name => self.claim_name, p_value => self.claim_c_value, p_write_null => TRUE);
    ELSIF self.claim_n_value IS NOT NULL
    THEN
      apex_json.write(p_name => self.claim_name, p_value => self.claim_n_value, p_write_null => TRUE);
    ELSIF self.claim_d_value IS NOT NULL
    THEN
      apex_json.write(p_name => self.claim_name, p_value => self.claim_d_value, p_write_null => TRUE);
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END json_str;
END;
/

