CREATE OR REPLACE TYPE UTL_API.utl_io_ot force AS OBJECT
(
  /**
  * Supports reading, creating of and writing to a CLOB (through the child object type UTL_CLOB_TYP) or a
  * OS text file (through child object type UTL_FILE_TYP).
  * 
  * %usage
  * This object type is not instantiable. It contains the attribute AC_FMODE to indicates whether the target
  * text object is a CLOB or a text OS file. The member routines are stubs wich are implemented in the child
  * object types UTL_CLOB_OT and UTL_FILE_OT. When called through this object type it wil result is a
  * NOT_SUPPORTED error message.
  * 
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * IO Mode:
  * %value FW write to file.
  * %value CW write to clob.
  * %value FR Read from file.
  * %value CR Read from clob.
  */
  fmode VARCHAR2(2),

  /**
  * Indicates whether the end of the file is reached.
  * %value Y EOF is reached.
  * %value N EOF is not reached yet.
  */
  eof_flg VARCHAR2(1),

  /**
  * Read buffer.
  */
  t_buffer apex_t_varchar2,

  /**
  * Number of lines in AT_BUFFER.
  */
  line_cnt INTEGER,

  /**
  * Current line in AT_BUFFER.
  */
  cur_line INTEGER,

  /**
  * Closes the instance.
  */
  MEMBER PROCEDURE close_file(SELF IN OUT utl_io_ot),

  /**
  * Returns the clob value.
  *
  * %return The CLOB value.
  */
  MEMBER FUNCTION getclob(SELF IN utl_io_ot) RETURN CLOB,

  /**
  * Returns the file path.
  *
  * %return The current file path.
  */
  MEMBER FUNCTION getpath(SELF IN utl_io_ot) RETURN VARCHAR2,

  /**
  * Writes IN_STR.
  *
  * %param in_str          String to write.
  * %param in_newline_flg Indicates whether a newline character must be written (Y/N).
  */
  MEMBER PROCEDURE put(SELF           IN OUT utl_io_ot,
                       in_str         IN VARCHAR2,
                       in_newline_flg IN VARCHAR2),

  /**
  * Reads the buffer (AT_BUFFER).
  */
  MEMBER PROCEDURE read_buffer(SELF IN OUT utl_io_ot),

  /**
  * Returns the next line from AT_BUFFER in PC_LINE.
  *
  * %param out_line Line read.
  *
  * %return {*} Y End of file is reached.
  *         {*} N End of ile is not reached yet.
  */
  MEMBER FUNCTION get_line(SELF     IN OUT utl_io_ot,
                           out_line OUT NOCOPY VARCHAR2) RETURN VARCHAR2
)
NOT INSTANTIABLE NOT FINAL
/

