CREATE OR REPLACE TYPE UTL_API.utl_tt_scope_ot force AS OBJECT
(
/**
  * Text template scope run time.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Text template scope (UK).
  */
  scope_code VARCHAR2(30),
  /**
  * Number of rows in T_ROWS.
  */
  row_idx INTEGER,
  /**
  * Table of variable/value sets or sub template calls.
  */
  t_rows utl_tt_row_ct,

  /**
  * Creates a new empty Text template scope run time instance.
  *
  * %param in_code Text template scope (UK).
  */
  CONSTRUCTOR FUNCTION utl_tt_scope_ot(in_code IN VARCHAR2) RETURN SELF AS RESULT,

  /**
  * Adds an empty  variable/value set to this instance.
  */
  MEMBER PROCEDURE add_row(SELF IN OUT utl_tt_scope_ot),

  /**
  * Adds a sub template call to this instance.
  *
  * %param in_tt_code Sub template to call.
  */
  MEMBER PROCEDURE add_row(SELF       IN OUT NOCOPY utl_tt_scope_ot,
                           in_tt_code IN VARCHAR2),
  MEMBER PROCEDURE add_var(SELF     IN OUT NOCOPY utl_tt_scope_ot,
                           in_name  IN VARCHAR2,
                           in_value IN VARCHAR2),
  /**
  * Generate this scope
  */
  MEMBER PROCEDURE generate(SELF                IN utl_tt_scope_ot,
                            in_file_idx         IN PLS_INTEGER,
                            in_templ_text       IN VARCHAR2,
                            in_boilerplate_text IN CLOB,
                            in_newline_flg      IN VARCHAR2)
)
/

