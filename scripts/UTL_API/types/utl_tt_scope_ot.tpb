CREATE OR REPLACE TYPE BODY UTL_API.utl_tt_scope_ot IS
  /*
  * Text Template Scopes
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Creates a new empty Text template scope run time instance.
  *
  * %param in_code Text template scope (UK).
  */
  CONSTRUCTOR FUNCTION utl_tt_scope_ot(in_code IN VARCHAR2) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_scope_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_tt_scope_ot';
  BEGIN
    self.scope_code := in_code;
    self.row_idx    := 0;
    self.t_rows     := utl_tt_row_ct();
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_tt_scope_ot;

  /**
  * Adds an empty  variable/value set to this instance.
  */
  MEMBER PROCEDURE add_row(SELF IN OUT NOCOPY utl_tt_scope_ot) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_scope_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'add_row';
  BEGIN
    self.row_idx := self.row_idx + 1;
    self.t_rows.extend(1);
    self.t_rows(self.row_idx) := utl_tt_row_ot();
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END add_row;

  /**
  * Adds a sub template call to this instance.
  *
  * %param in_tt_code Sub template to call.
  */
  MEMBER PROCEDURE add_row(SELF       IN OUT NOCOPY utl_tt_scope_ot,
                           in_tt_code IN VARCHAR2) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_scope_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'add_row';
  BEGIN
    self.row_idx := self.row_idx + 1;
    self.t_rows.extend(1);
    self.t_rows(self.row_idx) := utl_tt_row_ot(in_tt_code => in_tt_code);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END add_row;

  /**
  * Adds a variable/value pair to this instance.
  *
  * %param in_name  Variable name.
  * %param in_value Variable value.
  */
  MEMBER PROCEDURE add_var(SELF     IN OUT NOCOPY utl_tt_scope_ot,
                           in_name  IN VARCHAR2,
                           in_value IN VARCHAR2) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'add_var';
  BEGIN
    self.t_rows(self.row_idx).add_var(in_name => in_name, in_value => in_value);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END add_var;

  /**
  * Generates the template line given.
  *
  * %param in_file_idx         Target UTL_IO object.
  * %param in_templ_text       Template
  * %param in_boilerplate_text Boilerplate text.
  * %param in_newline_flg      Write a newline character after the genarated text (Y/N).
  */
  MEMBER PROCEDURE generate(SELF                IN utl_tt_scope_ot,
                            in_file_idx         IN PLS_INTEGER,
                            in_templ_text       IN VARCHAR2,
                            in_boilerplate_text IN CLOB,
                            in_newline_flg      IN VARCHAR2) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_scope_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'generate';
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_file_idx', in_value => in_file_idx);
    utl_log.set_par(in_name => 'self.scope_code', in_value => self.scope_code);
    utl_log.set_par(in_name => 'in_templ_text', in_value => in_templ_text);
    utl_log.set_par(in_name => 'in_boilerplate_text', in_value => in_boilerplate_text);
    utl_log.set_par(in_name => 'in_newline_flg', in_value => in_newline_flg);
    utl_log.close_entry;
    --
    FOR idx IN 1 .. self.row_idx
    LOOP
      t_rows(idx).generate(in_file_idx         => in_file_idx,
                           in_templ_text       => in_templ_text,
                           in_boilerplate_text => in_boilerplate_text,
                           in_newline_flg      => in_newline_flg);
    END LOOP;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.close_entry;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END generate;
END;
/

