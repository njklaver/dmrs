CREATE OR REPLACE TYPE UTL_API.utl_jwt_claim_ot force AS OBJECT
(
/**
  * JWT Claim.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */
/**
  * Claim name.
  */
  claim_name VARCHAR2(10),
/**
  * Claim value (VARCHAR2).
  */
  claim_c_value VARCHAR2(4000),
/**
  * Claim value (NUMBER).
  */
  claim_n_value NUMBER,
/**
  * Claim value (DATE).
  */
  claim_d_value DATE,

/**
  * Creates a new JWT Claim instance (VARCHAR2).
  *
  * %param in_claim_name  Claim name.
  * %param in_claim_value Claim value.
  */
  CONSTRUCTOR FUNCTION utl_jwt_claim_ot(in_claim_name  IN VARCHAR2,
                                        in_claim_value IN VARCHAR2) RETURN SELF AS RESULT,

/**
  * Creates a new JWT Claim instance (NUMBER).
  *
  * %param in_claim_name  Claim name.
  * %param in_claim_value Claim value.
  */
  CONSTRUCTOR FUNCTION utl_jwt_claim_ot(in_claim_name  IN VARCHAR2,
                                        in_claim_value IN NUMBER) RETURN SELF AS RESULT,

/**
  * Creates a new JWT Claim instance (DATE).
  *
  * %param in_claim_name  Claim name.
  * %param in_claim_value Claim value.
  */
  CONSTRUCTOR FUNCTION utl_jwt_claim_ot(in_claim_name  IN VARCHAR2,
                                        in_claim_value IN DATE) RETURN SELF AS RESULT,

/**
  * Adds this claim to the JWT.
  */
  MEMBER PROCEDURE json_str(SELF IN utl_jwt_claim_ot)
)
/

