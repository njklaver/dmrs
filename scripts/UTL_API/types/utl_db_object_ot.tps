create or replace type utl_api.utl_db_object_ot force as object
(
  /**
  * DB object names
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Object name.
  */
  object_name VARCHAR2(256),

  /**
  * Procedure name.
  */
  procedure_name VARCHAR2(256)
)
/

