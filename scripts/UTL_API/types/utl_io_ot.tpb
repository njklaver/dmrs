CREATE OR REPLACE TYPE BODY UTL_API.utl_io_ot IS
  /*
  * Supports reading, creating of and writing to a CLOB (through the child object type UTL_CLOB_TYP) or a
  * OS text file (through child object type UTL_FILE_TYP).
  * 
  * %usage
  * This object type is not instantiable. It contains the attribute AC_FMODE to indicates whether the target
  * text object is a CLOB or a text OS file. The member routines are stubs wich are implemented in the child
  * object types UTL_CLOB_OT and UTL_FILE_OT. When called through this object type it wil result is a
  * NOT_SUPPORTED error message.
  * 
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Closes the instance.
  */
  MEMBER PROCEDURE close_file(SELF IN OUT utl_io_ot) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_io_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'close_file';
  BEGIN
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  END close_file;
  /**
  * Returns the clob-value.
  *
  * %return The clob-value.
  */
  MEMBER FUNCTION getclob(SELF IN utl_io_ot) RETURN CLOB IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_io_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'getclob';
  BEGIN
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  END getclob;
  /**
  * Returns the file path.
  *
  * %return The file path.
  */
  MEMBER FUNCTION getpath(SELF IN utl_io_ot) RETURN VARCHAR2 IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_io_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'getpath';
  BEGIN
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  END getpath;
  /**
  * Writes IN_STR.
  *
  * %param in_str          String to write.
  * %param in_newline_flg Indicates whether a newline character must be written (Y/N).
  *
  * %raises utl_public_types.e_fatal when called here.
  */
  MEMBER PROCEDURE put(SELF           IN OUT utl_io_ot,
                       in_str         IN VARCHAR2,
                       in_newline_flg IN VARCHAR2) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_io_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'getpath';
  BEGIN
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  END put;
  /**
  * Reads the buffer (AT_BUFFER).
  *
  * %raises utl_public_types.e_fatal when called here.
  */
  MEMBER PROCEDURE read_buffer(SELF IN OUT utl_io_ot) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_io_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'getpath';
  BEGIN
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  END read_buffer;
  /**
  * Returns the next line from AT_BUFFER in PC_LINE.
  *
  * %param out_line Line read.
  *
  * %return {*} Y End of file is reached.
  *         {*} N End of ile is not reached yet.
  */
  MEMBER FUNCTION get_line(SELF     IN OUT utl_io_ot,
                           out_line OUT NOCOPY VARCHAR2) RETURN VARCHAR2 IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_io_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_line';
    --
    -- l_line Line read.
    l_line utl_public_types.text_type;
  BEGIN
    --
    -- Read the buffer (when needed).
    IF self.eof_flg = 'N' AND
       (self.line_cnt = 0 OR self.cur_line = self.line_cnt)
    THEN
      self.line_cnt := 0;
      self.read_buffer();
    END IF;
    --
    IF self.cur_line < self.line_cnt
    THEN
      self.cur_line := self.cur_line + 1;
      l_line        := self.t_buffer(self.cur_line);
    END IF;
    --
    out_line := l_line;
    RETURN(CASE WHEN l_line IS NULL THEN self.eof_flg ELSE 'N' END);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END get_line;
END;
/

