CREATE OR REPLACE TYPE UTL_API.utl_db_objects_ct force
  /**
  * Collection of UTL_DB_OBJECT_OT instances
  *
  * %author  Nico J. Klaver
  */
IS TABLE OF UTL_DB_OBJECT_OT;
/

