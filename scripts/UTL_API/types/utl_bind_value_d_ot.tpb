CREATE OR REPLACE TYPE BODY UTL_API.utl_bind_value_d_ot IS
  /*
  * UTL bind variable name/value pair. (DATE)
  * 
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /*
  * Creates a new date bind variable instance.
  *
  * %param in_var_name Bind variable name.
  * %param in_value    Value to be set.
  */
  CONSTRUCTOR FUNCTION utl_bind_value_d_ot(in_var_name IN VARCHAR2,
                                           in_value    IN DATE) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_bind_value_d_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_bind_value_d_ot';
  BEGIN
    self.var_name := in_var_name;
    self.value    := in_value;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_bind_value_d_ot;
  /*
  * Sets the bind variable value (using DBMS_SQL.BIND_VARIABLE) for the cursor supplied.
  *
  * %param in_cursor_id UTL_SQL cursor identifier.
  */
  OVERRIDING MEMBER PROCEDURE set_value(SELF         IN utl_bind_value_d_ot,
                                        in_cursor_id IN VARCHAR2) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_bind_value_d_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'set_value';
    --
    -- ln_cursor dbms_sql cursor to handle.
    l_cursor_nr utl_sql.cursor_type;
  BEGIN
    utl_sql.get_cursor(in_cursor_id => in_cursor_id, out_cursor_nr => l_cursor_nr);
    dbms_sql.bind_variable(c => l_cursor_nr, NAME => ':' || self.var_name, value => self.value);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END set_value;
  /**
  * Returns the bind variable value.
  *
  * %return The bind variable value.
  */
  OVERRIDING MEMBER FUNCTION get_d_value(SELF IN utl_bind_value_d_ot) RETURN DATE IS
  BEGIN
    RETURN self.value;
  END get_d_value;
  /**
  * Returns the bind variable string value.
  *
  * %return The bind variable string value.
  */
  OVERRIDING MEMBER FUNCTION get_value(SELF IN utl_bind_value_d_ot) RETURN VARCHAR2 IS
  BEGIN
    RETURN utl_date.date_to_canonical(self.value);
  END get_value;
END;
/

