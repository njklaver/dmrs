CREATE OR REPLACE TYPE UTL_API.utl_bind_value_n_ot force UNDER utl_bind_value_ot
(
  /**
  * UTL bind variable name/value pair. (NUMBER)
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Bind variable value.
  */
  value NUMBER,

  /**
  * Creates a new date bind variable instance.
  *
  * %param in_var_name Bind variable name.
  * %param in_value    Value to be set.
  */
  CONSTRUCTOR FUNCTION utl_bind_value_n_ot(in_var_name IN VARCHAR2,
                                           in_value    IN NUMBER) RETURN SELF AS RESULT,

  /**
  * Sets the bind variable value (using DBMS_SQL.BIND_VARIABLE) for the cursor supplied.
  *
  * %param in_cursor_id UTL_SQL cursor identifier.
  */
  OVERRIDING MEMBER PROCEDURE set_value(SELF         IN utl_bind_value_n_ot,
                                        in_cursor_id IN VARCHAR2),

  /**
  * Returns the bind variable value.
  *
  * %return The bind variable value.
  */
  OVERRIDING MEMBER FUNCTION get_n_value(SELF IN utl_bind_value_n_ot) RETURN NUMBER,

  /**
  * Returns the bind variable string value.
  *
  * %return The bind variable string value.
  */
  OVERRIDING MEMBER FUNCTION get_value(SELF IN utl_bind_value_n_ot) RETURN VARCHAR2
)
/

