create or replace type utl_api.utl_error_message_ot force as object
(
  /**
  * Error message
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */
  /**
  * PK.
  */
  id NUMBER,
  /**
  * Error message.
  */
  error_msg VARCHAR2(4000),
  /**
  * Master (PK).
  */
  message_master_id NUMBER
)
/

