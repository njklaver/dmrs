CREATE OR REPLACE TYPE UTL_API.utl_bind_value_c_ot force UNDER utl_bind_value_ot
(
/**
  * UTL bind variable name/value pair. (VARCHAR2).
  * 
  * %author Nico Klaver -  nklaver@itium.nl
  */
/**
  * Bind variable value.
  */
  value VARCHAR2(4000),

/**
  * Creates a new varchar2 bind variable instance.
  *
  * %param in_var_name Bind variable name.
  * %param in_value    Value to be set.
  */
  CONSTRUCTOR FUNCTION utl_bind_value_c_ot(in_var_name IN VARCHAR2,
                                           in_value    IN VARCHAR2) RETURN SELF AS RESULT,

/**
  * Sets the bind variable value (using DBMS_SQL.BIND_VARIABLE) for the cursor supplied.
  *
  * %param in_cursor_id    utl_sql cursor identifier.
  */
  OVERRIDING MEMBER PROCEDURE set_value(SELF         IN utl_bind_value_c_ot,
                                        in_cursor_id IN VARCHAR2),

/**
  * Returns the bind variable value.
  *
  * %return The bind variable value.
  */
  OVERRIDING MEMBER FUNCTION get_c_value(SELF IN utl_bind_value_c_ot) RETURN VARCHAR2,

/**
  * Returns the bind variable string value.
  *
  * %return The bind variable string value.
  */
  OVERRIDING MEMBER FUNCTION get_value(SELF IN utl_bind_value_c_ot) RETURN VARCHAR2
)
/

