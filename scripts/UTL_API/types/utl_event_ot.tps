CREATE OR REPLACE TYPE UTL_API.utl_event_ot force AS OBJECT
(
/**
  * Application AQ Event.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

/**
  * Event type.
  */
  event_type VARCHAR2(30),

/**
  * Event JWT.
  */
  event_jwt utl_string_ct,

/**
  * Event payload
  */
  event_payload CLOB,

/**
  * Creates a new UTL_EVENT_OT instance.
  *
  * %param in_event_type     Event type.
  * %param in_event_jwt      Event JWT.
  * %param in_event_payload  Event payload.
  */
  CONSTRUCTOR FUNCTION utl_event_ot(in_event_type    IN VARCHAR2,
                                    in_event_jwt     IN VARCHAR2,
                                    in_event_payload IN CLOB) RETURN SELF AS RESULT,
/**
  * Returns the event type for this event.
  *
  * %return The event type for this event.
  */
  MEMBER FUNCTION get_event_type(SELF IN utl_event_ot) RETURN VARCHAR2,

/**
  * Returns the payload.
  *
  * %return The payload for this event
  */
  MEMBER FUNCTION get_event_payload(SELF IN utl_event_ot) RETURN VARCHAR2
)
/

