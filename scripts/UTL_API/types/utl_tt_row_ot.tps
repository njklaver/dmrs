CREATE OR REPLACE TYPE UTL_API.utl_tt_row_ot force AS OBJECT
(
/**
  * Text Template scope row
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */
  var_idx INTEGER,
  t_vars  utl_tt_var_ct,
  tt_code VARCHAR2(61),

  CONSTRUCTOR FUNCTION utl_tt_row_ot RETURN SELF AS RESULT,
  CONSTRUCTOR FUNCTION utl_tt_row_ot(in_tt_code IN VARCHAR2) RETURN SELF AS RESULT,
  MEMBER PROCEDURE add_var(SELF     IN OUT NOCOPY utl_tt_row_ot,
                           in_name  IN VARCHAR2,
                           in_value IN VARCHAR2),
  /**
  * Generates this row.
  */
  MEMBER PROCEDURE generate(SELF                IN utl_tt_row_ot,
                            in_file_idx         IN PLS_INTEGER,
                            in_templ_text       IN VARCHAR2,
                            in_boilerplate_text IN CLOB,
                            in_newline_flg      IN VARCHAR2)
)
/

