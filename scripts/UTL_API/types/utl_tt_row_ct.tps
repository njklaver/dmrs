CREATE OR REPLACE TYPE UTL_API.utl_tt_row_ct force
  /**
  * Text Template Scope Rows
  *
  * %author  Nico J. Klaver
  */
IS TABLE OF utl_tt_row_ot;
/

