CREATE OR REPLACE TYPE BODY UTL_API.utl_file_ot IS
  /*
  * This object type supports reading, creating of or writing to an OS text file.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Creates a new UTL_FILE_OT instance.
  *
  * %param in_dir       OS directory (Oracle directory object) where the file must be created.
  * %param in_fnm       Filename to create.
  * %param in_open_mode Specifies how the file is opened. Modes include:
  *                     {*} r read text
  *                     {*} w write text
  */
  CONSTRUCTOR FUNCTION utl_file_ot(in_dir       IN VARCHAR2,
                                   in_fnm       IN VARCHAR2,
                                   in_open_mode IN VARCHAR2) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_file_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_file_ot';
    --
    -- r_file_type UTL_FILE.FILE TYPE for the text file.
    r_file_type utl_file.file_type;
  BEGIN
    --
    -- Open the file. Save the OS path in PATH.
    -- SAVE r_file_type.file_id and r_file_type.datatype in the attributes an_file_id and an_datatype
    -- for later usage in close_file and put.
    <<open_file>>
    BEGIN
      self.path     := utl_io.get_path(in_dir, in_fnm);
      r_file_type   := utl_file.fopen(location     => in_dir,
                                      filename     => in_fnm,
                                      open_mode    => in_open_mode,
                                      max_linesize => utl_public_types.co_max_text_len);
      self.file_id  := r_file_type.id;
      self.datatype := r_file_type.datatype;
      self.fmode    := 'F' || upper(in_open_mode);
      self.eof_flg  := 'N';
      self.line_cnt := 0;
    EXCEPTION
      WHEN utl_file.invalid_path THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_PATH'),
                                TRUE);
      WHEN utl_file.invalid_mode THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_MODE'),
                                TRUE);
      WHEN utl_file.invalid_operation THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_OPERATION'),
                                TRUE);
      WHEN utl_file.invalid_maxlinesize THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_MAXLINESIZE'),
                                TRUE);
    END open_file;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_file_ot;
  /**
  * Closes this instance.
  */
  OVERRIDING MEMBER PROCEDURE close_file(SELF IN OUT utl_file_ot) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_file_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'close_file';
    --
    -- r_file_type UTL_FILE.FILE_TYPE for the text file.
    r_file_type utl_file.file_type;
  BEGIN
    r_file_type.id       := self.file_id;
    r_file_type.datatype := self.datatype;
    --
    <<fclose>>
    BEGIN
      utl_file.fclose(file => r_file_type);
      self.file_id  := NULL;
      self.datatype := NULL;
      self.fmode    := NULL;
      self.path     := NULL;
      IF self.fmode = utl_io.co_fr
      THEN
        self.t_buffer.delete();
      END IF;
      self.line_cnt := 0;
    EXCEPTION
      WHEN utl_file.write_error THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'WRITE_ERROR'),
                                TRUE);
      WHEN utl_file.invalid_filehandle THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_FILEHANDLE'),
                                TRUE);
    END fclose;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END close_file;
  /**
  * Returns the OS path for this instance.
  *
  * %return The OS path for this instance.
  */
  OVERRIDING MEMBER FUNCTION getpath(SELF IN utl_file_ot) RETURN VARCHAR2 IS
  BEGIN
    RETURN self.path;
  END getpath;
  /**
  * Writes pc_str to this file.
  *
  * %param in_str         String to write.
  * %param in_newline_flg Indicates whether a newline character must be apended (Y/N).
  */
  OVERRIDING MEMBER PROCEDURE put(SELF           IN OUT utl_file_ot,
                                  in_str         IN VARCHAR2,
                                  in_newline_flg IN VARCHAR2) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_file_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'put';
    --
    -- r_file_type UTL_FILE.FILE_TYPE for the text file.
    r_file_type utl_file.file_type;
  BEGIN
    r_file_type.id       := self.file_id;
    r_file_type.datatype := self.datatype;
    --
    IF self.fmode <> utl_io.co_fw
    THEN
      raise_application_error(-20000,
                              utl_msg.io_error(in_module     => co_type_name,
                                               in_unit       => co_unit,
                                               in_io_context => self.path,
                                               in_file_error => 'INVALID_OPERATION'),
                              TRUE);
    END IF;
    --
    IF in_newline_flg NOT IN ('N', 'Y')
    THEN
      raise_application_error(-20000,
                              utl_msg.invalid(in_item => 'in_newline_flg', in_value => in_newline_flg, in_value_exp => '(N, Y)'),
                              TRUE);
    END IF;
    --
    <<put_line>>
    BEGIN
      IF in_newline_flg = 'Y'
      THEN
        utl_file.put_line(file => r_file_type, buffer => in_str);
        utl_file.fflush(file => r_file_type);
      ELSE
        utl_file.put(file => r_file_type, buffer => in_str);
      END IF;
    EXCEPTION
      WHEN utl_file.invalid_operation THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_OPERATION'),
                                TRUE);
      WHEN utl_file.write_error THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'WRITE_ERROR'),
                                TRUE);
      WHEN utl_file.invalid_filehandle THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_FILEHANDLE'),
                                TRUE);
    END put_line;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END put;
  /**
  * Reads the buffer from this instance.
  */
  OVERRIDING MEMBER PROCEDURE read_buffer(SELF IN OUT utl_file_ot) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_file_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'put';
    --
    -- r_file_type UTL_FILE.FILE_TYPE for the text file.
    -- l_line      Line read.
    r_file_type utl_file.file_type;
    l_line      utl_public_types.text_type;
  BEGIN
    r_file_type.id       := self.file_id;
    r_file_type.datatype := self.datatype;
    --
    IF self.fmode <> utl_io.co_fr
    THEN
      raise_application_error(-20000,
                              utl_msg.io_error(in_module     => co_type_name,
                                               in_unit       => co_unit,
                                               in_io_context => self.path,
                                               in_file_error => 'INVALID_OPERATION'),
                              TRUE);
    END IF;
    --
    <<lines>>
    BEGIN
      self.t_buffer := apex_t_varchar2();
      self.t_buffer. EXTEND(utl_io.co_max_buf_len + 1);
      self.cur_line := 0;
      LOOP
        utl_file.get_line(file => r_file_type, buffer => l_line);
        self.line_cnt := self.line_cnt + 1;
        self.t_buffer(self.line_cnt) := l_line;
        --
        EXIT WHEN self.line_cnt = utl_io.co_max_buf_len;
      END LOOP;
    EXCEPTION
      WHEN no_data_found THEN
        self.eof_flg := 'Y';
      WHEN value_error THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'LINE_TOO_LONG'),
                                TRUE);
      WHEN utl_file.invalid_operation THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_OPERATION'),
                                TRUE);
      WHEN utl_file.read_error THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'READ_ERROR'),
                                TRUE);
      
      WHEN utl_file.invalid_filehandle THEN
        raise_application_error(-20000,
                                utl_msg.io_error(in_module     => co_type_name,
                                                 in_unit       => co_unit,
                                                 in_io_context => self.path,
                                                 in_file_error => 'INVALID_FILEHANDLE'),
                                TRUE);
    END lines;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END read_buffer;
END;
/

