CREATE OR REPLACE TYPE BODY UTL_API.utl_jwt_ot IS
  /*
  * JSAN Web Token
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Creates a new UTL_JWT_OT instance by decoding the given JWT string.
  * %param in_jwt JWT to decode.
  *
  * %return A new UTL_JWT_OT instance.
  */
  CONSTRUCTOR FUNCTION utl_jwt_ot(in_jwt IN VARCHAR2) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_jwt', in_value => in_jwt);
    utl_log.close_entry;
    --
    <<parse_jwt>>
    BEGIN
      utl_jwt_util.verify(in_jwt => in_jwt);
      self.jwt_claims    := utl_jwt_util.jwt_claims(in_jwt => in_jwt);
      self.return_status := 'S';
    EXCEPTION
      WHEN utl_jwt_util.e_verification_failed THEN
        utl_log.leave(in_module => co_type_name, in_unit => co_unit);
        utl_log.set_par(in_name => 'exception', in_value => 'utl_jwt_util.e_verification_failed');
        utl_log.close_entry;
        self.return_status := 'E';
    END parse_jwt;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'self.return_status', in_value => self.return_status);
    utl_log.close_entry;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_jwt_ot;
  /**
  * Creates a new UTL_JWT_OT instance.
  *
  * %param in_iss Issuer.
  * %param in_iat Issued at (POSIX-date).
  * %param in_jti Unique identifier.
  * %param in_exp Expired at (POSIX-date).
  *
  * %return A new utl_jwt_ot instance.
  */
  CONSTRUCTOR FUNCTION utl_jwt_ot(in_iss IN VARCHAR2,
                                  in_iat IN NUMBER,
                                  in_jti IN VARCHAR2,
                                  in_exp IN NUMBER DEFAULT NULL) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_iss', in_value => in_iss);
    utl_log.set_par(in_name => 'in_iat', in_value => in_iat);
    utl_log.set_par(in_name => 'in_jti', in_value => in_jti);
    utl_log.set_par(in_name => 'in_exp', in_value => in_exp);
    utl_log.close_entry;
  
    self.jwt_claims := utl_jwt_claim_ct();
    --
    self.set_claim('iss', in_iss);
    self.set_claim('iat', in_iat);
    self.set_claim('exp', in_exp);
    self.set_claim('jti', in_jti);
    self.return_status := 'S';
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'self.return_status', in_value => self.return_status);
    utl_log.close_entry;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_jwt_ot;
  /**
  * Sets a JWT claim.
  *
  * %param in_claim_name   Claim name.
  * %param in_claim_value  Claim value.
  * %param in_null         Always set the claim even when the value is null.
  */
  MEMBER PROCEDURE set_claim(SELF           IN OUT utl_jwt_ot,
                             in_claim_name  IN VARCHAR2,
                             in_claim_value IN VARCHAR2,
                             in_null        IN BOOLEAN DEFAULT FALSE) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'set_claim';
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_claim_name', in_value => in_claim_name);
    utl_log.set_par(in_name => 'in_claim_value', in_value => in_claim_value);
    utl_log.set_par(in_name => 'in_null', in_value => in_null);
    utl_log.close_entry;
    --
    IF in_null OR
       in_claim_value IS NOT NULL
    THEN
      self.jwt_claims.extend(1);
      self.jwt_claims(self.jwt_claims.last()) := utl_jwt_claim_ot(in_claim_name, in_claim_value => in_claim_value);
    END IF;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.close_entry;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END set_claim;
  /**
  * Sets a JWT claim.
  *
  * %param in_claim_name   Claim name.
  * %param in_claim_value  Claim value.
  * %param in_null   Always set the claim even when the value is null.
  */
  MEMBER PROCEDURE set_claim(SELF           IN OUT utl_jwt_ot,
                             in_claim_name  IN VARCHAR2,
                             in_claim_value IN NUMBER,
                             in_null        IN BOOLEAN DEFAULT FALSE) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'set_claim';
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_claim_name', in_value => in_claim_name);
    utl_log.set_par(in_name => 'in_claim_value', in_value => in_claim_value);
    utl_log.set_par(in_name => 'in_null', in_value => in_null);
    utl_log.close_entry;
    --
    IF in_null OR
       in_claim_value IS NOT NULL
    THEN
      self.jwt_claims.extend(1);
      self.jwt_claims(self.jwt_claims.last()) := utl_jwt_claim_ot(in_claim_name => in_claim_name, in_claim_value => in_claim_value);
    END IF;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.close_entry;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END set_claim;
  /**
  * Sets a JWT claim.
  *
  * %param in_claim_name   Claim name.
  * %param in_claim_value  Claim value.
  * %param in_null   Always set the claim even when the value is null.
  */
  MEMBER PROCEDURE set_claim(SELF           IN OUT utl_jwt_ot,
                             in_claim_name  IN VARCHAR2,
                             in_claim_value IN DATE,
                             in_null        IN BOOLEAN DEFAULT FALSE) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'set_claim';
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_claim_name', in_value => in_claim_name);
    utl_log.set_par(in_name => 'in_claim_value', in_value => in_claim_value);
    utl_log.set_par(in_name => 'in_null', in_value => in_null);
    utl_log.close_entry;
    --
    IF in_null OR
       in_claim_value IS NOT NULL
    THEN
      self.jwt_claims.extend(1);
      self.jwt_claims(self.jwt_claims.last()) := utl_jwt_claim_ot(in_claim_name => in_claim_name, in_claim_value => in_claim_value);
    END IF;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.close_entry;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END set_claim;

  /**
  * Returns the JWT string for this instance.
  *
  * %return The JWT string for this instance. 
  */
  MEMBER FUNCTION get_jwt(SELF IN utl_jwt_ot) RETURN VARCHAR2 IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_jwt';
    --
    -- l_header  JWT-header
    -- l_payload JWT-payload
    -- l_jwt     The JWT string.
    l_header  utl_public_types.str_type;
    l_payload utl_public_types.str_type;
    l_jwt     utl_public_types.str_type;
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.close_entry;
    --
    l_header  := utl_jwt_util.jwt_header();
    l_payload := utl_jwt_util.jwt_payload(in_jwt_claims => self.jwt_claims);
    --
    l_jwt := utl_jwt_util.sign_jwt(in_header => l_header, in_payload => l_payload);
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'l_jwt', in_value => l_jwt);
    utl_log.close_entry;
    --
    RETURN l_jwt;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END get_jwt;
  /**
  * Returns the value for the claim given (VARCHAR2).
  *
  * %param in_claim_name The claim to handle.
  * %param in_must_exist Indicates whether the claim must exist.
  *
  * %return The claim value (VARCHAR2). 
  */
  MEMBER FUNCTION get_claim_c(SELF          IN utl_jwt_ot,
                              in_claim_name IN VARCHAR2,
                              in_must_exist IN BOOLEAN DEFAULT TRUE) RETURN VARCHAR2 IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_claim_c';
    --
    -- l_value The claim value.
    l_value VARCHAR2(4000);
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_claim_name', in_value => in_claim_name);
    utl_log.set_par(in_name => 'in_must_exist', in_value => in_must_exist);
    utl_log.close_entry;
    --
    <<get_value>>
    BEGIN
      SELECT c.claim_c_value
      INTO   l_value
      FROM   TABLE(self.jwt_claims) c
      WHERE  c.claim_name = in_claim_name;
    EXCEPTION
      WHEN no_data_found THEN
        IF in_must_exist
        THEN
          RAISE;
        END IF;
    END get_value;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'l_value', in_value => l_value);
    utl_log.close_entry;
    --
    RETURN l_value;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END get_claim_c;
  /**
  * Returns the value for the claim given (NUMBER).
  *
  * %param in_claim_name  The claim to handle.
  * %param in_must_exist  Indicates whether the claim must exist.
  *
  * %return The claim value (NUMBER). 
  */
  MEMBER FUNCTION get_claim_n(SELF          IN utl_jwt_ot,
                              in_claim_name IN VARCHAR2,
                              in_must_exist IN BOOLEAN DEFAULT TRUE) RETURN NUMBER IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_claim_n';
    --
    -- l_value The claim value.
    l_value NUMBER;
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_claim_name', in_value => in_claim_name);
    utl_log.set_par(in_name => 'in_must_exist', in_value => in_must_exist);
    utl_log.close_entry;
    --
    <<get_value>>
    BEGIN
      SELECT c.claim_n_value
      INTO   l_value
      FROM   TABLE(self.jwt_claims) c
      WHERE  c.claim_name = in_claim_name;
    EXCEPTION
      WHEN no_data_found THEN
        IF in_must_exist
        THEN
          RAISE;
        END IF;
    END get_value;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'l_value', in_value => l_value);
    utl_log.close_entry;
    --
    RETURN l_value;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END get_claim_n;
  /**
  * Returns the value for the claim given (DATE).
  *
  * %param in_claim_name  The claim to handle.
  * %param in_must_exist  Indicates whether the claim must exist.
  *
  * %return The claim value (DATE). 
  */
  MEMBER FUNCTION get_claim_d(SELF          IN utl_jwt_ot,
                              in_claim_name IN VARCHAR2,
                              in_must_exist IN BOOLEAN DEFAULT TRUE) RETURN DATE IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_claim_c';
    --
    -- l_value The claim value.
    l_value DATE;
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_claim_name', in_value => in_claim_name);
    utl_log.set_par(in_name => 'in_must_exist', in_value => in_must_exist);
    utl_log.close_entry;
    --
    <<get_value>>
    BEGIN
      SELECT c.claim_d_value
      INTO   l_value
      FROM   TABLE(self.jwt_claims) c
      WHERE  c.claim_name = in_claim_name;
    EXCEPTION
      WHEN no_data_found THEN
        IF in_must_exist
        THEN
          RAISE;
        END IF;
    END get_value;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'l_value', in_value => l_value);
    utl_log.close_entry;
    --
    RETURN l_value;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END get_claim_d;
  /**
  * Checks whether this token is expired.
  *
  * %return {*} Y The token is expired
  *         {*} N The token is not expired.
  */
  MEMBER FUNCTION is_expired(SELF IN utl_jwt_ot) RETURN VARCHAR2 IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_jwt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'is_expired';
    --
    -- l_exp        Experation date (POSIX).
    -- l_is_expired TRUE when this token is expired.
    l_exp        INTEGER;
    l_is_expired BOOLEAN;
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.close_entry;
    --
    l_exp := self.get_claim_n('exp', FALSE);
    --
    l_is_expired := l_exp IS NOT NULL AND SYSDATE >= utl_date.from_posix(l_exp);
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'l_exp', in_value => l_exp);
    utl_log.set_par(in_name => 'l_is_expired', in_value => l_is_expired);
    utl_log.close_entry;
    --
    RETURN CASE WHEN l_is_expired THEN 'Y' ELSE 'N' END;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END is_expired;
END;
/

