CREATE OR REPLACE TYPE UTL_API.utl_tt_var_ot force AS OBJECT
(
/**
  * Text Template Variable instance.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */
  name  VARCHAR2(30),
  value VARCHAR2(4000),

  CONSTRUCTOR FUNCTION utl_tt_var_ot(in_name  IN VARCHAR2,
                                     in_value IN VARCHAR2) RETURN SELF AS RESULT
)
/

