CREATE OR REPLACE TYPE BODY UTL_API.utl_tt_var_ot IS
  /*
  * Text Template Variable instance.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  CONSTRUCTOR FUNCTION utl_tt_var_ot(in_name  IN VARCHAR2,
                                     in_value IN VARCHAR2) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_var_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_tt_var_ot';
  BEGIN
    self.name  := in_name;
    self.value := in_value;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_tt_var_ot;
END;
/

