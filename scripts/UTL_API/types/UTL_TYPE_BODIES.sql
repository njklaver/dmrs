prompt PL/SQL Developer Export User Objects for user UTL_API@DOCKER-DB1
prompt Created by njklaver on zaterdag 4 mei 2019
set define off
spool TT_TYPE_BODIES.log

prompt
prompt Creating type body UTL_BIND_VALUE_C_OT
prompt ======================================
prompt
@@utl_bind_value_c_ot.tpb
prompt
prompt Creating type body UTL_BIND_VALUE_D_OT
prompt ======================================
prompt
@@utl_bind_value_d_ot.tpb
prompt
prompt Creating type body UTL_BIND_VALUE_NTAB_OT
prompt =========================================
prompt
@@utl_bind_value_ntab_ot.tpb
prompt
prompt Creating type body UTL_BIND_VALUE_N_OT
prompt ======================================
prompt
@@utl_bind_value_n_ot.tpb
prompt
prompt Creating type body UTL_BIND_VALUE_OT
prompt ====================================
prompt
@@utl_bind_value_ot.tpb
prompt
prompt Creating type body UTL_CLOB_OT
prompt ==============================
prompt
@@utl_clob_ot.tpb
prompt
prompt Creating type body UTL_EVENT_OT
prompt ===============================
prompt
@@utl_event_ot.tpb
prompt
prompt Creating type body UTL_FILE_OT
prompt ==============================
prompt
@@utl_file_ot.tpb
prompt
prompt Creating type body UTL_IO_OT
prompt ============================
prompt
@@utl_io_ot.tpb
prompt
prompt Creating type body UTL_JWT_CLAIM_OT
prompt ===================================
prompt
@@utl_jwt_claim_ot.tpb
prompt
prompt Creating type body UTL_JWT_OT
prompt =============================
prompt
@@utl_jwt_ot.tpb
prompt
prompt Creating type body UTL_TT_OT
prompt ============================
prompt
@@utl_tt_ot.tpb
prompt
prompt Creating type body UTL_TT_ROW_OT
prompt ================================
prompt
@@utl_tt_row_ot.tpb
prompt
prompt Creating type body UTL_TT_SCOPE_OT
prompt ==================================
prompt
@@utl_tt_scope_ot.tpb
prompt
prompt Creating type body UTL_TT_VAR_OT
prompt ================================
prompt
@@utl_tt_var_ot.tpb

prompt Done
spool off
set define on
