CREATE OR REPLACE TYPE UTL_API.utl_error_messages_ct force
  /**
  * Error messages
  *
  * %author  Nico J. Klaver
  */
IS TABLE OF utl_error_message_ot;
/

