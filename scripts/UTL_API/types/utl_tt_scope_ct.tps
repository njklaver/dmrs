CREATE OR REPLACE TYPE UTL_API.utl_tt_scope_ct force
  /**
  * Text template scopes
  *
  * %author  Nico J. Klaver
  */
IS TABLE OF utl_tt_scope_ot;
/

