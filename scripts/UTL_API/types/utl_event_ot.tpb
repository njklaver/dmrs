CREATE OR REPLACE TYPE BODY UTL_API.utl_event_ot IS
  /*
  * Application AQ Event.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Creates a new UTL_EVENT_OT instance.
  *
  * %param in_event_type     Event type.
  * %param in_event_jwt      Event JWT.
  * %param in_event_payload  Event payload.
  */
  CONSTRUCTOR FUNCTION utl_event_ot(in_event_type    IN VARCHAR2,
                                    in_event_jwt     IN VARCHAR2,
                                    in_event_payload IN CLOB) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_event_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_event_ot';
  BEGIN
    self.event_type    := in_event_type;
    self.event_jwt     := utl_string.split_str(in_str => in_event_jwt);
    self.event_payload := in_event_payload;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_event_ot;
  /**
  * Returns the event type for this event.
  *
  * %return The event type for this event.
  */
  MEMBER FUNCTION get_event_type(SELF IN utl_event_ot) RETURN VARCHAR2 IS
  BEGIN
    RETURN self.event_type;
  END get_event_type;
  /**
  * Returns the payload.
  *
  * %return The payload for this event
  */
  MEMBER FUNCTION get_event_payload(SELF IN utl_event_ot) RETURN VARCHAR2 IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_event_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_event_ot';
  BEGIN
    RETURN utl_string.join_str(self.event_jwt);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END get_event_payload;
END;
/

