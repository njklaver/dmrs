prompt PL/SQL Developer Export User Objects for user UTL_API@DOCKER-DB1
prompt Created by njklaver on zaterdag 4 mei 2019
set define off
spool TT_TYPES.log

prompt
prompt Creating type UTL_BIND_VALUE_OT
prompt ===============================
prompt
@@utl_bind_value_ot.tps
prompt
prompt Creating type UTL_BIND_VALUE_C_OT
prompt =================================
prompt
@@utl_bind_value_c_ot.tps
prompt
prompt Creating type UTL_BIND_VALUE_D_OT
prompt =================================
prompt
@@utl_bind_value_d_ot.tps
prompt
prompt Creating type UTL_BIND_VALUE_NTAB_OT
prompt ====================================
prompt
@@utl_bind_value_ntab_ot.tps
prompt
prompt Creating type UTL_BIND_VALUE_N_OT
prompt =================================
prompt
@@utl_bind_value_n_ot.tps
prompt
prompt Creating type UTL_IO_OT
prompt =======================
prompt
@@utl_io_ot.tps
prompt
prompt Creating type UTL_CLOB_OT
prompt =========================
prompt
@@utl_clob_ot.tps
prompt
prompt Creating type UTL_DB_OBJECT_OT
prompt ==============================
prompt
@@utl_db_object_ot.tps
prompt
prompt Creating type UTL_DB_OBJECTS_CT
prompt ===============================
prompt
@@utl_db_objects_ct.tps
prompt
prompt Creating type UTL_ERROR_MESSAGE_OT
prompt ==================================
prompt
@@utl_error_message_ot.tps
prompt
prompt Creating type UTL_ERROR_MESSAGES_CT
prompt ===================================
prompt
@@utl_error_messages_ct.tps
prompt
prompt Creating type UTL_STRING_CT
prompt ===========================
prompt
@@utl_string_ct.tps
prompt
prompt Creating type UTL_EVENT_OT
prompt ==========================
prompt
@@utl_event_ot.tps
prompt
prompt Creating type UTL_FILE_OT
prompt =========================
prompt
@@utl_file_ot.tps
prompt
prompt Creating type UTL_HISTORY_OT
prompt ============================
prompt
@@utl_history_ot.tps
prompt
prompt Creating type UTL_HISTORY_CT
prompt ============================
prompt
@@utl_history_ct.tps
prompt
prompt Creating type UTL_JOB_REQUEST_OT
prompt ================================
prompt
@@utl_job_request_ot.tps
prompt
prompt Creating type UTL_JWT_CLAIM_OT
prompt ==============================
prompt
@@utl_jwt_claim_ot.tps
prompt
prompt Creating type UTL_JWT_CLAIM_CT
prompt ==============================
prompt
@@utl_jwt_claim_ct.tps
prompt
prompt Creating type UTL_JWT_OT
prompt ========================
prompt
@@utl_jwt_ot.tps
prompt
prompt Creating type UTL_TT_VAR_OT
prompt ===========================
prompt
@@utl_tt_var_ot.tps
prompt
prompt Creating type UTL_TT_VAR_CT
prompt ===========================
prompt
@@utl_tt_var_ct.tps
prompt
prompt Creating type UTL_TT_ROW_OT
prompt ===========================
prompt
@@utl_tt_row_ot.tps
prompt
prompt Creating type UTL_TT_ROW_CT
prompt ===========================
prompt
@@utl_tt_row_ct.tps
prompt
prompt Creating type UTL_TT_SCOPE_OT
prompt =============================
prompt
@@utl_tt_scope_ot.tps
prompt
prompt Creating type UTL_TT_SCOPE_CT
prompt =============================
prompt
@@utl_tt_scope_ct.tps
prompt
prompt Creating type UTL_TT_OT
prompt =======================
prompt
@@utl_tt_ot.tps

prompt Done
spool off
set define on
