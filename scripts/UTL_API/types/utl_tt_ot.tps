CREATE OR REPLACE TYPE UTL_API.utl_tt_ot force AS OBJECT
(
/**
  * Text Template
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

/**
  * Text Template Code.
  */
  code VARCHAR2(30),
/**
  * Number of Text Template scopes. Also the current scope entry.
  */
  scope_idx INTEGER,
/**
  * Text Template Scopes for this Text Template.
  */
  t_scopes utl_tt_scope_ct,

/**
  * Creates a new UTL_TT_OT instance.
  *
  * %param in_code Text template code.
  *
  * %return the created UTL_TT_OT instance.
  */
  CONSTRUCTOR FUNCTION utl_tt_ot(in_code IN VARCHAR2) RETURN SELF AS RESULT,
/**
  * Adds a scope to this Text Template.
  *
  * in_code Scope (UK) to add.
  */
  MEMBER PROCEDURE add_scope(SELF    IN OUT NOCOPY utl_tt_ot,
                             in_code IN VARCHAR2),
/**
  * Adds a new row to the current scope of this Text Template.
  */
  MEMBER PROCEDURE add_row(SELF IN OUT NOCOPY utl_tt_ot),
/**
  * Adds a new sub Text Template to the current scope of this Text Template.
  *
  * %param in_tt_code Sub template to call.
  */
  MEMBER PROCEDURE add_row(SELF       IN OUT NOCOPY utl_tt_ot,
                           in_tt_code IN VARCHAR2),

/**
  * Adds a variable value pair to the current row in the the current scope of this Text Template.
  *
  * %param in_name  Variable name.
  * %param in_value Variable value.
  */
  MEMBER PROCEDURE add_var(SELF     IN OUT NOCOPY utl_tt_ot,
                           in_name  IN VARCHAR2,
                           in_value IN VARCHAR2),

/**
  * Generates this Text Template.
  *
  * %param in_file_idx Generation target (UTL_IO).
  */
  MEMBER PROCEDURE generate(SELF        IN utl_tt_ot,
                            in_file_idx IN PLS_INTEGER)
)
/

