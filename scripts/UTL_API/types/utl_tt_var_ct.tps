create or replace type utl_api.utl_tt_var_ct force
  /**
  * Scope Variables
  *
  * %author  Nico J. Klaver
  */
is table OF utl_tt_var_ot;
/

