create or replace type body utl_api.utl_bind_value_ot is
  /*
  * UTL bind variable name/value pair.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  --
  -- Member program units ------------------------------------------------------
  /*
  * Sets the bind variable value for the cursor supplied.
  *
  * %param in_cursor_id    UTL_SQL cursor id.
  */
  member procedure set_value(self            in utl_bind_value_ot,
                             in_cursor_id    in VARCHAR2) is
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_bind_value_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'set_value';
  BEGIN
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  end set_value;
  /*
  * Returns the bind variable value (VARCHAR2).
  *
  * %return Bind variable value.
  */
  member function get_c_value(self in utl_bind_value_ot) return varchar2 is
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_bind_value_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_c_value';
  begin
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  end get_c_value;
  /*
  * Returns the bind variable value (NUMBER).
  *
  * %return Bind variable value.
  */
  member function get_n_value(self in utl_bind_value_ot) return number is
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_bind_value_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_n_value';
  begin
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  end get_n_value;
  /*
  * Returns the bind variable value (NUMBER).
  *
  * %return Bind variable value.
  */
  member function get_ntab_value(self in utl_bind_value_ot) return apex_t_number is
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_bind_value_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_ntab_value';
  begin
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  end get_ntab_value;
  /*
  * Returns the bind variable value (DATE).
  *
  * %return Bind variable value.
  */
  member function get_d_value(self in utl_bind_value_ot) return date is
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_bind_value_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_d_value';
  begin
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  end get_d_value;
  /*
  * Returns the string value.
  *
  * %return The bind variable value converted to VARCHAR2.
  *
  * %raises <code>utl_public_types.e_fatal</code> when called here.
  */
  member function get_value(self in utl_bind_value_ot) return varchar2 is
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_bind_value_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'get_value';
  begin
    raise_application_error(-20000, utl_msg.not_supported(in_type_name => co_type_name, in_unit => co_unit), TRUE);
  end get_value;
  /*
  * Returns the bind variable name.
  *
  * %return Bind variable name.
  */
  member function get_var_name(self in utl_bind_value_ot) return varchar2 is
  begin
    return self.var_name;
  end get_var_name;
end;
/

