CREATE OR REPLACE TYPE BODY UTL_API.utl_tt_ot IS
  /*
  * Text Template
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */

  CONSTRUCTOR FUNCTION utl_tt_ot(in_code IN VARCHAR2) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_tt_ot';
  BEGIN
    self.code      := in_code;
    self.scope_idx := 0;
    self.t_scopes  := utl_tt_scope_ct();
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_tt_ot;

  MEMBER PROCEDURE add_scope(SELF    IN OUT NOCOPY utl_tt_ot,
                             in_code IN VARCHAR2) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'add_scope';
  BEGIN
    self.scope_idx := self.scope_idx + 1;
    self.t_scopes.extend(1);
    self.t_scopes(self.scope_idx) := utl_tt_scope_ot(in_code => in_code);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END add_scope;

  MEMBER PROCEDURE add_row(SELF IN OUT NOCOPY utl_tt_ot) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'add_row';
  BEGIN
    self.t_scopes(self.scope_idx).add_row;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END add_row;

  MEMBER PROCEDURE add_row(SELF       IN OUT NOCOPY utl_tt_ot,
                           in_tt_code IN VARCHAR2) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_row_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'add_row';
  BEGIN
    self.t_scopes(self.scope_idx).add_row(in_tt_code => in_tt_code);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END add_row;

  MEMBER PROCEDURE add_var(SELF     IN OUT NOCOPY utl_tt_ot,
                           in_name  IN VARCHAR2,
                           in_value IN VARCHAR2) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'add_var';
  BEGIN
    self.t_scopes(self.scope_idx).add_var(in_name => in_name, in_value => in_value);
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END add_var;
  /**
  * Generates this template
  */
  MEMBER PROCEDURE generate(SELF        IN utl_tt_ot,
                            in_file_idx IN PLS_INTEGER) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_tt_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'generate';
    --
    --
    -- Retrieve the Text Template Lines for the Text Template pr_text_templ_info
    CURSOR c_ttl(p_template_code IN utl_public_types.code_type) IS
      SELECT ttl.*
      FROM   utl_text_templ_lines_v ttl
      WHERE  ttl.template_code = p_template_code
      ORDER  BY ttl.line_nr;
    --
    -- t_ttl_type Resultset for C_TTL.
    TYPE t_ttl_type IS TABLE OF c_ttl%ROWTYPE INDEX BY utl_public_types.idx_type;
    --
    -- l_cnt Number of records in T_TTL.
    -- t_ttl Resultset for C_TTL.
    l_line_cnt  PLS_INTEGER;
    t_ttl       t_ttl_type;
    t_scope_idx utl_text_template_util.t_scope_idx_type;
  BEGIN
    utl_log.enter(in_module => co_type_name, in_unit => co_unit);
    utl_log.set_par(in_name => 'in_file_idx', in_value => in_file_idx);
    utl_log.set_par(in_name => 'self.code', in_value => self.code);
    utl_log.close_entry;
    --
    OPEN c_ttl(p_template_code => self.code);
    FETCH c_ttl BULK COLLECT
      INTO t_ttl;
    CLOSE c_ttl;
    t_scope_idx := utl_text_template_util.scope_idx(in_tt => SELF);
    l_line_cnt  := t_ttl.count;
    FOR line_idx IN 1 .. l_line_cnt
    LOOP
      t_scope_idx(t_ttl(line_idx).scope_code).generate(in_file_idx         => in_file_idx,
                                                       in_templ_text       => t_ttl(line_idx).templ_text,
                                                       in_boilerplate_text => t_ttl(line_idx).boilerplate_text,
                                                       in_newline_flg      => t_ttl(line_idx).newline_flg);
    END LOOP;
    --
    utl_log.leave(in_module => co_type_name, in_unit => co_unit);
    utl_log.close_entry;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END generate;
END;
/

