create or replace type utl_api.utl_job_request_ot force as object
(
  /**
  * Job Request Queue Payload.
  *
  * %author Nico Klaver -  nklaver@itium.nl
  */
  
  request_id NUMBER,
  created TIMESTAMP WITH LOCAL TIME ZONE
)
/

