CREATE OR REPLACE TYPE BODY UTL_API.utl_clob_ot IS
  /*
  * This object type supports creating of, writing to or reading from a clob.
  *
  * Author Nico Klaver -  nklaver@itium.nl
  */

  /**
  * Creates an empty instance for writing.
  *
  * %param in_newline New line sequence to be used (LF or CRLF).
  */
  CONSTRUCTOR FUNCTION utl_clob_ot(in_newline IN VARCHAR2 DEFAULT NULL) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_clob_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_clob_ot';
  BEGIN
    self.clobval := '';
    self.newline := coalesce(in_newline, utl_io.co_newline);
    IF self.newline NOT IN (utl_io.co_newline, utl_tcp.crlf)
    THEN
      raise_application_error(-20000,
                              utl_msg.invalid(in_item      => 'pc_newline',
                                              in_value     => rawtohex(in_newline),
                                              in_value_exp => '(0D10, 10)'),
                              TRUE);
    END IF;
    --
    self.fmode := utl_io.co_cw;
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_clob_ot;
  /**
  * Creates an instance for reading.
  *
  * %param in_clobval clob to read.
  */
  CONSTRUCTOR FUNCTION utl_clob_ot(in_clobval IN CLOB) RETURN SELF AS RESULT IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_clob_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'utl_clob_ot';
  BEGIN
    self.clobval  := in_clobval;
    self.newline  := utl_io.co_newline;
    self.fmode    := utl_io.co_cr;
    self.pos      := 1;
    self.eof_flg  := 'N';
    self.line_cnt := 0;
    --
    RETURN;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END utl_clob_ot;
  /**
  * Closes this instance.
  */
  OVERRIDING MEMBER PROCEDURE close_file(SELF IN OUT utl_clob_ot) IS
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_clob_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'close_file';
  BEGIN
    self.clobval := NULL;
    self.fmode   := NULL;
    self.pos     := NULL;
    self.eof_flg := NULL;
    IF self.fmode = utl_io.co_cr
    THEN
      self.t_buffer.delete;
    END IF;
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END close_file;
  /**
  * Returns CLOBVAL.
  *
  * %return CLOBVAL
  */
  OVERRIDING MEMBER FUNCTION getclob(SELF IN utl_clob_ot) RETURN CLOB IS
  BEGIN
    RETURN self.clobval;
  END getclob;
  /**
  * Writes IN_STR to the clob.
  *
  * %param in_str         String to write.
  * %param in_newline_flg Indicates whether a newline character must be apended (Y/N).
  */
  OVERRIDING MEMBER PROCEDURE put(SELF           IN OUT utl_clob_ot,
                                  in_str         IN VARCHAR2,
                                  in_newline_flg IN VARCHAR2) IS
    --
    --
    -- co_type_name Type name. Used in logging.
    -- co_unit      Program unit name. Used in logging.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_clob_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'put';
  BEGIN
    IF self.fmode <> utl_io.co_cw
    THEN
      raise_application_error(-20000,
                              utl_msg.io_error(in_module     => co_type_name,
                                               in_unit       => co_unit,
                                               in_io_context => '[clob, ' || self.fmode || ']',
                                               in_file_error => 'INVALID_OPERATION'),
                              TRUE);
    END IF;
    --
    IF in_newline_flg NOT IN ('N', 'Y')
    THEN
      raise_application_error(-20000,
                              utl_msg.invalid(in_item => 'in_newline_flg', in_value => in_newline_flg, in_value_exp => '(N, Y)'),
                              TRUE);
    END IF;
    --
    -- NoFormat Start
    self.clobval := self.clobval || case when in_newline_flg = 'Y' then in_str || self.newline else in_str end;
    -- NoFormat End
  EXCEPTION
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END put;
  /**
  * Reads the buffer from this instance.
  */
  OVERRIDING MEMBER PROCEDURE read_buffer(SELF IN OUT utl_clob_ot) IS
    --
    -- co_type_name Type name.
    -- co_unit      Program unit name.
    co_type_name CONSTANT utl_public_types.object_name_type := 'utl_clob_ot';
    co_unit      CONSTANT utl_public_types.object_name_type := 'read_buffer';
    --
    -- lt_lines Local buffer.
    t_lines utl_collections.t_str_type;
  BEGIN
    IF self.fmode <> utl_io.co_cr
    THEN
      raise_application_error(-20000,
                              utl_msg.io_error(in_module     => co_type_name,
                                               in_unit       => co_unit,
                                               in_io_context => '[clob, ' || self.fmode || ']',
                                               in_file_error => 'INVALID_OPERATION'),
                              TRUE);
    END IF;
    --
    IF self.clobval IS NOT NULL AND
       self.eof_flg = 'N'
    THEN
      utl_string.get_lines(in_clobval => self.clobval, in_pos => self.pos, out_lines => t_lines);
      --
      self.t_buffer := apex_t_varchar2();
      self.t_buffer.extend(utl_io.co_max_buf_len + 1);
      --
      self.line_cnt := t_lines.count;
      self.cur_line := 0;
      FOR idx IN 1 .. self.line_cnt
      LOOP
        self.t_buffer(idx) := t_lines(idx);
      END LOOP;
    ELSE
      self.pos := -1;
    END IF;
    -- NoFormat Start
    self.eof_flg := case when self.pos = -1 then  'Y' else 'N' end;
    -- NoFormat End
  EXCEPTION
    WHEN value_error THEN
      raise_application_error(-20000,
                              utl_msg.io_error(in_module     => co_type_name,
                                               in_unit       => co_unit,
                                               in_io_context => '[clob, ' || self.fmode || ']',
                                               in_file_error => 'LINE_TOO_LONG'),
                              TRUE);
    WHEN OTHERS THEN
      utl_log.excep(in_module      => co_type_name,
                    in_unit        => co_unit,
                    in_errmsg      => SQLERRM,
                    in_call_stack  => utl_exception_stack.call_stack,
                    in_error_stack => utl_exception_stack.error_stack,
                    in_back_trace  => utl_exception_stack.back_trace);
      RAISE;
  END read_buffer;

END;
/

