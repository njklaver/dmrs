CREATE OR REPLACE TYPE UTL_API.utl_history_ct FORCE
  /**
  * Record history.
  *
  * %author  Nico J. Klaver
  */
IS TABLE OF utl_history_ot;
/

