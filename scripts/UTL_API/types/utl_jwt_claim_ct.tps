CREATE OR REPLACE TYPE UTL_API.utl_jwt_claim_ct force
  /**
  * JWT Claims
  *
  * %author  Nico J. Klaver
  */
IS TABLE OF utl_jwt_claim_ot;
/

