/**
* Initializes the DML events entities 
*
* Can only be run in an empty database.
*/
declare
   l_created timestamp with local time zone;
   l_cnt     number;
begin
   select count(*)
   into   l_cnt
   from   utl_dm_events dev
   where  dev.id <> -1;
   if l_cnt = 0
   then
      --
      -- Delete all DML events 
      delete from utl_data.utl_dml_events_b;
   
      l_created := localtimestamp;
      insert into utl_dml_events
         (id
         ,det_id
         ,created
         ,created_by)
      values
         (-1
         ,-1
         ,l_created
         ,'UTL_API');
      --
      insert into utl_dml_event_types
         (id
         ,code
         ,table_alias
         ,ind_approval
         ,reg_id
         ,created)
      values
         (-1
         ,'DET'
         ,'DET'
         ,0
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types_tl
         (id
         ,det_id
         ,meaning
         ,description
         ,lang
         ,reg_id
         ,created)
      values
         (-1
         ,-1
         ,'Table alias DET'
         ,'Table alias DET'
         ,'en'
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types
         (id
         ,code
         ,table_alias
         ,ind_approval
         ,reg_id
         ,created)
      values
         (-2
         ,'LUT'
         ,'LUT'
         ,0
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types_tl
         (id
         ,det_id
         ,meaning
         ,description
         ,lang
         ,reg_id
         ,created)
      values
         (-2
         ,-2
         ,'Table alias LUT'
         ,'Table alias LUT'
         ,'en'
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types
         (id
         ,code
         ,table_alias
         ,ind_approval
         ,reg_id
         ,created)
      values
         (-3
         ,'LUP'
         ,'LUP'
         ,0
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types_tl
         (id
         ,det_id
         ,meaning
         ,description
         ,lang
         ,reg_id
         ,created)
      values
         (-3
         ,-3
         ,'Table alias LUP'
         ,'Table alias LUP'
         ,'en'
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types
         (id
         ,code
         ,table_alias
         ,operation
         ,ind_approval
         ,reg_id
         ,created)
      values
         (to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
         ,'APEX'
         ,null
         ,'APEX'
         ,0
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types_tl
         (id
         ,det_id
         ,meaning
         ,description
         ,lang
         ,reg_id
         ,created)
         select to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
               ,det.id
               ,'DML by APEX'
               ,'DML by APEX'
               ,'en'
               ,-1
               ,l_created
         from   utl_dml_event_types det
         where  det.code = 'APEX';
   
      insert into utl_dml_event_types
         (id
         ,code
         ,table_alias
         ,operation
         ,ind_approval
         ,reg_id
         ,created)
      values
         (to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
         ,'SCRIPT'
         ,null
         ,'SCRIPT'
         ,0
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types_tl
         (id
         ,det_id
         ,meaning
         ,description
         ,lang
         ,reg_id
         ,created)
         select to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
               ,det.id
               ,'DML by script'
               ,'DML by script'
               ,'en'
               ,-1
               ,l_created
         from   utl_dml_event_types det
         where  det.code = 'SCRIPT';
   
      insert into utl_dml_event_types
         (id
         ,code
         ,table_alias
         ,operation
         ,ind_approval
         ,reg_id
         ,created)
      values
         (to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
         ,'AJAX'
         ,null
         ,'AJAX'
         ,0
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types_tl
         (id
         ,det_id
         ,meaning
         ,description
         ,lang
         ,reg_id
         ,created)
         select to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
               ,det.id
               ,'DML by ajax'
               ,'DML by ajax'
               ,'en'
               ,-1
               ,l_created
         from   utl_dml_event_types det
         where  det.code = 'AJAX';
      --
      insert into utl_dml_event_types
         (id
         ,code
         ,table_alias
         ,operation
         ,ind_approval
         ,reg_id
         ,created)
      values
         (to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
         ,'REST'
         ,null
         ,'REST'
         ,0
         ,-1
         ,l_created);
   
      insert into utl_dml_event_types_tl
         (id
         ,det_id
         ,meaning
         ,description
         ,lang
         ,reg_id
         ,created)
         select to_number(sys_guid(), 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
               ,det.id
               ,'DML by rest service'
               ,'DML by rest service'
               ,'en'
               ,-1
               ,l_created
         from   utl_dml_event_types det
         where  det.code = 'REST';
      commit;
   end if;
exception
   when others then
      rollback;
      raise;
end;
/
