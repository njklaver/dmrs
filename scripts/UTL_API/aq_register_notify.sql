DECLARE
  l_queue_name VARCHAR2(30) := 'UTL_API.UTL_EVENT_QUEUE';
BEGIN
  dbms_aqadm.add_subscriber(queue_name => l_queue_name, subscriber => sys.aq$_agent('recipient', NULL, NULL));

  dbms_aq.register(sys.aq$_reg_info_list(sys.aq$_reg_info(l_queue_name || ':recipient',
                                                          dbms_aq.namespace_aq,
                                                          'plsql://utl_api.utl_event_api.notify_event',
                                                          hextoraw('ff'))),
                   1);
END;
/
