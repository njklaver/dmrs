PL/SQL Developer Test script 3.0
17
DECLARE
  t_requests apex_t_number;
BEGIN
  utl_db_context.init_debug(in_app_id => 102, in_user_id => 'UTL_ADMIN', in_lang => 'en', in_debug_run_code => 'SUBMIT');
  --
  SELECT id
  BULK   COLLECT
  INTO   t_requests
  FROM   utl_job_requests req;
  -- Call the procedure
  FOR idx IN 1 .. t_requests.count
  LOOP
    utl_job_request_manager.submit(in_request_id => t_requests(idx));
  END LOOP;
  COMMIT;
  utl_log.flush;
END;
1
in_request_id
0
-4
0
