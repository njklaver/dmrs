/**
 * Initializes p8 module.
 * @function init
 * 
 * @param {Object} debug APEX debug module
 * 
 * @example
 * init(apex.debug);
 **/
 const init = (debug) => {
   debug.log("p8.init");
 };
 
 export default {
   init
 };
 