/**
 * Initializes p4 module.
 * @function init
 * 
 * @param {Object} debug APEX debug module
 * 
 * @example
 * init(apex.debug);
 **/
 const init = (debug) => {
   debug.log("p4.init");
 };
 
 export default {
   init
 };
 