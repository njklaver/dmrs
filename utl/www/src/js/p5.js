/**
 * Initializes p5 module.
 * @function init
 * 
 * @param {Object} debug APEX debug module
 * 
 * @example
 * init(apex.debug);
 **/
 const init = (debug) => {
   debug.log("p5.init");
 };
 
 export default {
   init
 };
 