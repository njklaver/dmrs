/**
 * Initializes p3 module.
 * @function init
 * 
 * @param {Object} debug APEX debug module
 * 
 * @example
 * init(apex.debug);
 **/
 const init = (debug) => {
   debug.log("p3.init");
 };
 
 export default {
   init
 };
 