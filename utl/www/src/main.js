
/**
 * Main module for APEX application UTL
 */
import p1$module from "./js/p1";
import p2$module from "./js/p2";
import p3$module from "./js/p3";
import p4$module from "./js/p4";
import p5$module from "./js/p5";
import p6$module from "./js/p6";
import p7$module from "./js/p7";
import p8$module from "./js/p8";
import igUtl$module from "./js/igUtl";


let ig;

/*
* Initializes JS for the application.
*/
apex.jQuery(document).ready(() => {
  const pageId = Number(document.getElementById("pFlowStepId").value);

  switch (pageId) {
    case 1:
      p1$module.init(apex.debug);
      break;
    case 2:
      p2$module.init(apex.debug);
      break;
    case 3:
      p3$module.init(apex.debug);
      break;
    case 4:
      ig = igUtl$module.init(apex.debug); 
      p4$module.init(apex.debug);
      break;
    case 5:
      p5$module.init(apex.debug);
      break;
    case 6:
      p6$module.init(apex.debug);
      break;
    case 7:
      ig = igUtl$module.init(apex.debug); 
      p7$module.init(apex.debug);
      break;
    case 8:
        p8$module.init(apex.debug);
        break;
    default:
      apex.debug.log("default: " + pageId);
  }
});

export {
  ig
}
